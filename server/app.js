/*
* @Author: comely-fox
* @Date:   2018-05-17 16:14:17
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-05-19 08:41:22
*/


const path = require( 'path' ),
      Koa = require( 'koa' ),
      KoaStatic = require( 'koa-static'),
      render = require( 'Koa-art-template'),
      bodyParser = require( 'koa-bodyparser' ),
      app = new Koa();

app.listen( 3000 );

// 处理post文本数据
app.use( bodyParser() );

// 配置art模板引擎
render( app, {
    root : path.join( __dirname, 'views' ),
    extname : '.art',
    debug : process.env.NODE_ENV !== 'production'
} );

// 配置session
const session = require( './module/session.js' );
session( app );


// 处理静态文件
app.use( KoaStatic('statics') );


// 404 处理
app.use( async ( ctx, next  ) => {
    await next();
    if ( ctx.status === 404 ){
        ctx.body = 404;
    }
} );


// 前台页
const index = require( './routes/index.js');

// 后台页
const admin = require( './routes/admin.js');

app.use( index.routes(), index.allowedMethods() );
app.use( admin.routes(), admin.allowedMethods() );


