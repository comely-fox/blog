/*
* @Author: comely-fox
* @Date:   2018-05-18 15:01:14
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-05-24 09:51:27
*/

const mysql = require( 'mysql' );

const db_config = require( './db_config.js' );

const pool = mysql.createPool( db_config );

exports.do = function( sql ){
    return new Promise( ( resolve, reject ) => {
        this.getConnection( ( err, connection) => {

            if ( err ){
                console.log( err )
                return;
            }
            connection.query( sql, function( err, result ) {

                if ( err ){
                    reject( err );
                }else{
                    resolve( result );
                }
                connection.release();
            } );

        } );
    } );
}.bind( pool );
