/*
* @Author: comely-fox
* @Date:   2018-05-26 09:20:27
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-23 10:34:30
*/


module.exports = function( result, option ){
    const data = {
        startRow        : 0,        // 当前页的第一条数据
        endRow          : 0,        // 当前页的最后一条数据
        firstPage       : 0,        // 第一页
        lastPage        : 0,        // 最后一页
        hasPreviousPage : false,    // 是否有上一页
        hasNextPage     : false,    // 是否有下一页
        isFirstPage     : true,     // 是否是第一页
        isLastPage      : false,    // 是否是最后一页
        pageNum         : 0,        // 现在是哪一页
        nextPage        : 0,        // 当前页的下一页
        prevPage        : 0,        // 当前页的上一页
        pageSize        : 0,        // 一页多少条数据
        size            : 0,        // 一页实际多少条数据
        pages           : 0,        // 总共多少页
        total           : 0,        // 总共多少条数据,
        list            : []        // 存储数据
    };
    const length = result.length;

    let { pageNum, pageSize, ctx } = option;

    pageNum  = Math.max( pageNum >> 0, 1 );   // 处理转换后小于1的情况, 默认为第1页

    pageSize = Math.min( pageSize >>> 0, length );  // 处理转换后小于0的情况, 默认为显示全部数据

    // 开始位置, 最大不能超过总数据长度
    const startPosition = Math.min( ( pageNum - 1 ) * pageSize, length );


    // 结束位置, 最大不能超过总数据长度
    const endPosition = Math.min( startPosition + pageSize, length );


    { /** * 正常情况下, 默认 * **/

        // 当前页的第一条数据
        data.startRow = startPosition + 1;

        // 当前页的最后一条数据
        data.endRow = endPosition;

        // 第一页
        data.firstPage = 1;

        // 最后一页
        data.lastPage = Math.ceil( length / pageSize );

        // 是否有上一页
        data.hasPreviousPage = false;

        // 是否有下一页
        data.hasNextPage = true;

        // 是否是第一页
        data.isFirstPage = true;

        // 是否是最后一页
        data.isLastPage = false;

        // 当前是哪一页
        data.pageNum = pageNum;

        // 当前页的上一页
        data.prevPage = pageNum - 1;

        // 当前页的下一页
        data.nextPage = pageNum + 1;

        // 一页默认显示多少条数据, 前台决定
        data.pageSize = pageSize;

        // 一页实际显示多少条数据
        data.size = endPosition - startPosition;

        // 总共多少页
        data.pages = Math.ceil( length / pageSize );

        // 总共多少条数据
        data.total = length;

    }

    // 当前页大于1的时候&&小于等于最后一页
    if ( pageNum > 1 && pageNum <= data.lastPage ){

        // 是否有上一页
        data.hasPreviousPage = true;

        // 是否是第一页
        data.isFirstPage = false;
    }

    if ( pageNum === data.lastPage ){

        // 是否有下一页
        data.hasNextPage = false;

        // 是否是最后一页
        data.isLastPage = true;

        // 当前页的下一页
        data.nextPage = 0;
    }

    // 非正常情况下, 要查询的数据超出查询到的数据
    if ( startPosition === length ){

        // 当前页的第一条数据
        data.startRow = 0;

        // 当前页的最后一条数据
        data.endRow = 0;

        // 是否有上一页
        data.hasPreviousPage = true;

        // 是否有下一页
        data.hasNextPage = false;

        // 是否是第一页
        data.isFirstPage = false;

        // 当前页的上一页
        data.prevPage = data.lastPage;

        // 当前页的下一页
        data.nextPage = 0;

    }

    // 填充数据
    for ( var i = startPosition; i < endPosition; i++ ){

        const item = result[ i ];
        if ( ctx ){

            item.loginUserQueryable = ctx.licence.query( item.username );
            item.loginUserEditable  = ctx.licence.edit( item.username );
            item.loginUserAddable   = ctx.licence.add( item.username );
            item.loginUserDeletable = ctx.licence.delete( item.username );
        }

        data.list.push( item );
    }
    return data;
}