/*
* @Author: comely-fox
* @Date:   2018-05-18 11:31:53
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-05-18 11:36:06
*/


module.exports = {
    'host' : 'localhost',
    'user' : 'root',
    'password' : 'root',
    'database' : 'blog'
};