/*
* @Author: comely-fox
* @Date:   2018-05-19 08:38:30
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-05-29 09:10:32
*/

const session = require( 'koa-session' );
const config = {
    key : 'sess_id',
    maxAge : 20 * 60 * 1000,
    overwrite: true,
    httpOnly: true,
    signed: true,
    rolling: false,
    renew: true
}

module.exports = function( app ){
    // session 密钥
    app.keys = [];
    for ( let i = 0; i < 100000; i++ ){
        app.keys.push( Math.random() + '' );
    }
    // 使用 session
    app.use( session( config, app ) );
}
