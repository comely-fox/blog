/*
* @Author: comely-fox
* @Date:   2018-05-18 16:10:04
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-05-18 16:17:50
*/


const crypto = require( 'crypto' );

module.exports = {
    sha1 : function( str ){
        const sha1 = crypto.createHash( 'sha1' );
        const secretKey = 'mangesecript?foxamindmi(jobjson)mvln';
        sha1.update( secretKey + str );
        return sha1.digest( 'hex' );
    }
}