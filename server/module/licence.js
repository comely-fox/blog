/*
* @Author: comely-fox
* @Date:   2018-06-14 10:28:31
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-15 16:05:30
*/

/**
 * [ 中间件, 检查用户的权限 ]
 * @param  {Object}   ctx  [ 上下文环境对象 ]
 * @param  {Function} next [ 继续 ]
 */
module.exports = async function( ctx, next ){

    ctx.licence = {
        isRoot( username){
            return +( username === 'admin' );
        }
    };

    if ( ctx.session.userInfo ){

        Object.assign( ctx.licence, {
            query(){
                return ctx.session.userInfo.queryable;
            },

            add(){
                return ctx.session.userInfo.addable;
            },

            delete( username ){
                return + ( ctx.session.userInfo.deletable && username !== ctx.session.userInfo.username && !this.isRoot( username ) );
            },

            edit( username ){
                return  + ( username === ctx.session.userInfo.username || ( ctx.session.userInfo.editable && !this.isRoot( username ) ) );
            }
        } );

    }
    await next();
};