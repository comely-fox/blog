/*
* @Author: comely-fox
* @Date:   2018-05-18 10:31:09
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-05-18 23:46:14
*/
/*
* @Author: comely-fox
* @Date:   2018-05-17 16:16:41
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-05-18 23:29:01
*/

const router = require( 'koa-router' )();


router.get( '/', async ctx => {
    ctx.body = 'index';
} );




module.exports = router;

