/*
* @Author: comely-fox
* @Date:   2018-05-18 11:05:36
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-21 18:26:27
*/


const router = require( 'koa-router' )();
// 引用数据库模块
const db = require( '../../module/db.js' );
// 引入加密模块
const secret = require( '../../module/secret.js' );

// 引用分页处理模块
const pagination = require( '../../module/pagination.js' );


const Licence = require( '../../module/licence.js' );

router.prefix( '/user' );

/**
 * 0   用户名不存在
 * 1   用户密码不正确
 * 2   限制登录
 * 200 成功
 * 300 提示
 */

router
    .get( '/',  async ( ctx, next ) => {

        // 检测是否登录
        if ( !ctx.session.sess_id ){

            await ctx.render( 'admin/login.art' );
        }
        // 已登录直接跳转后台首页
        else{
            await ctx.redirect( '/admin/' );

        }
    })

    // 中间件, 处理权限
    .use( Licence )

    // 用户登录验证处理
    .post( '/login', async ( ctx ) => {

        const res      = {},
              { username, password } = ctx.request.body,

              // 根据用户名查询数据
              result   = await db.do( `SELECT username FROM admin WHERE username='${username}'` );

        // 未查询到数据, 管理员账户不存在
        if ( result.length === 0 ){

            res.msg    = '管理员不存在';
            res.status = 0;
            res.title  = 'username';

        }

        // 查询到数据, 管理员存在, 可以继续检验
        else{

            // 查询用户名, 密码是否正确
            const result = await db.do( `SELECT id,login_time,add_time,status,queryable,addable,editable,deletable FROM admin WHERE username='${username}' AND password='${secret.sha1(password)}'` );

            // 未查询到数据, 密码不正确, 验证不通过
            if ( result.length === 0 ){

                res.msg    = '管理员密码不正确';
                res.title  = 'password';
                res.status = 1;
            }

            // 查询到数据, 密码正确
            else{

                // 关闭状态, 不能登录, 验证不通过
                if( result[ 0 ].status === 0 ){

                    // 响应的数据
                    res.msg = '此管理员帐号已被限制登录';
                    res.status = 2;

                }

                // 打开状态, 可以登录, 验证通过
                else {

                    // 响应的数据
                    res.data   = {
                        username : username,
                        isRoot   : ctx.licence.isRoot( username ),
                        ...result[ 0 ]
                    };
                    res.status = 200;
                    res.msg    = '登录成功';

                    // 设置session, 保存用户登录状态信息
                    ctx.session.sess_id   = result[ 0 ].id;
                    ctx.session.userInfo = { username, ...result[ 0 ] };

                    // 更新登录时间
                    await db.do( `UPDATE admin SET login_time=${(new Date()).getTime()} WHERE username='${username}'` );

                }

            }

        }

        // 响应回数据
        ctx.body = res;

    })

    // 注销登录
    .get( '/logout', async ctx => {
        const res   = {};

        // 清除session
        ctx.session = null;

        res.status  = 200;
        res.msg     = "注销成功";

        // 响应的数据
        ctx.body    = res;

    })

    // 登录检查
    .get( '/checkLogin', async ctx => {
        const res      = {},
              userInfo = ctx.session.userInfo;

        // 检测session里是否用户信息, 有已经登录状态
        if ( userInfo ){

            // 响应的数据
            res.status = 200;
            res.data = { ...userInfo };
            res.msg = "管理员已登录";

        }

        // 否则, 未登录状态
        else{

            // 响应的数据
            res.status = 0;
            res.msg = "管理员未登录, 请登录后再操作";

        }

        ctx.body = res;

    })

    // 查询所有管理员
    .get( '/list', async ctx => {
        const res = {};

        let { pageNum, pageSize, orderBy }  = ctx.query;

        pageNum  = + pageNum;
        pageSize = + pageSize;

        // 排序方式, 默认使用升序
        if ( orderBy === 'default' || !orderBy ){

            orderBy = 'ASC';

        }


        // 查询所有管理员数据
        const result = await db.do( `SELECT id, username, login_time, add_time, status, queryable, addable, editable, deletable FROM admin ORDER BY add_time ${orderBy}` );

        // 根据前台传过来的参数, 返回数据
        const data = pagination( result, { pageNum : pageNum, pageSize : pageSize, ctx: ctx }  );
        console.log( data )
        // 响应的数据
        res.data   = data;
        res.status = 200;
        res.msg    = '获取管理员信息成功';

        ctx.body = res;

    })

    // 查询管理员是否可用
    .post( '/findUsernameExists',  async ctx => {
        const res      = {},
              username = ctx.request.body.username.trim(),

              // 查询账户
              result   = await db.do( `SELECT id, username, login_time, add_time, status FROM admin WHERE username='${username}'` );

        // 没有查询到, 可以使用
        if ( result.length === 0 ){

            // 响应的数据
            res.msg = '管理员帐号可以使用';
            res.status = 200;

        }

        // 查询到了, 不可以使用
        else{

            // 响应的数据
            res.msg = '管理员已存在';
            res.status = 300;

        }

        ctx.body = res;

    })

    // 添加管理员帐号
    .post( '/add',  async ctx => {
        const res = {};

        let { username, password, status, queryable, editable, addable, deletable } = ctx.request.body;

        // 处理空白, 字符串强制转为数字
        username  = username.trim();
        password  = password.trim();
        status    = + status;
        queryable = + queryable;
        editable  = + editable;
        addable   = + addable;
        deletable = + deletable;

        // 没有添加权限
        if ( !ctx.licence.add() ){

            res.msg    = `账户添加失败, 没有添加管理员账户的权限`;
            res.status = 2;

        }else{

            const login_time = (new Date()).getTime(),
                  add_time   = (new Date()).getTime();

            // 不是超级管理员, 只有查询权限
            if ( !ctx.licence.isRoot( ctx.session.userInfo.username ) ){

                queryable = 1;
                addable   = 0;
                editable  = 0;
                deletable = 0;

            }

            const sql = `INSERT INTO admin (username, password, login_time, add_time, status, queryable, addable, editable, deletable)
                VALUES ('${username}', '${secret.sha1(password)}', ${login_time}, ${add_time}, ${status}, ${queryable}, ${addable}, ${editable}, ${deletable})`;

            const result = await db.do( sql );

            // 受影响的行数
            if ( result.affectedRows > 0 ){

                // 响应的数据
                res.msg = '增加管理员账户成功';
                res.status = 200;
                res.data = { username, add_time, status, queryable, addable, editable, deletable } ;

            }else{

                // 响应的数据
                res.msg = '增加管理员账户失败';
                res.status = 0;

            }
        }

        ctx.body = res;

    })

    // 获取用户信息
    .get( '/getUserInfo', async ctx => {
        const res              = {},
              { id, username } = ctx.query;

        let result = [ ];

        if ( !id && !username ){

            res.msg = '非法参数';
            res.status = 1;

        }else{

            // 默认使用带 用户名 的参数请求方式
            let sql = `SELECT username, status, queryable, addable, editable, deletable FROM admin WHERE username='${username}'`;

            // 如果请求参数有 id 的情况下, 使用带 id 的参数请求方式
            if ( id ){

                sql = `SELECT username, status, queryable, addable, editable, deletable FROM admin WHERE id='${id}'`;

            }

            // 获取用户信息
            const result = await db.do( sql );

            // 查询到数据
            if ( result.length > 0 ){

                // 响应的数据
                res.data = {  ...result[ 0 ] };
                res.msg = '信息获取成功';
                res.status = 200;

            }

            // 没有查询到数据
            else{

                // 响应的数据
                res.msg = '信息获取失败, 此用户信息不存在';
                res.status = 0;

            }

        }

        ctx.body = res;
    })

    // 更新管理员密码
    .post( '/updateAdminPass', async ctx => {
        const res = {};

        const { oldPassword, newPassword } = ctx.request.body;
        const username                     = ctx.session.userInfo.username;

        // 查询原密码与帐号是否匹配
        const result = await db.do( `SELECT id FROM admin WHERE username='${username}' AND password='${secret.sha1( oldPassword.trim() )}'` );

        // 原密码与帐号匹配到数据
        if ( result[ 0 ] ){

            const id = result[ 0 ].id;

            // 更新管理员密码
            const result = await db.do( `UPDATE admin SET password='${secret.sha1( newPassword.trim() )}' WHERE id=${id}` );

            // 受影响的行数, 更新成功
            if ( result.affectedRows > 0 ){

                // 响应的数据
                res.status = 200;
                res.msg = '密码更新成功';

            }

            // 更新失败
            else{

                // 响应的数据
                res.status = 0;
                res.msg = '密码更新失败, 此管理员已经不存在';

            }

        }

        // 没有匹配到
        else{

            res.status = 1;
            res.msg = '原始密码不正确';

        }

        ctx.body = res;

    })

    // 更新某一个管理员用户资料
    .post( '/updateSomeUserInfo', async ctx => {
        const res = {};

        let { username, password, status, queryable, editable, addable, deletable } = ctx.request.body;

        newPassword  = password.trim();
        status       = + status;
        queryable    = + queryable;
        editable     = + editable;
        addable      = + addable;
        deletable    = + deletable;

        // 当前登录用户, 没有编辑此账户的权限
        if ( !ctx.licence.edit( username ) ){

            res.msg    = `管理员用户资料更新失败, 没有编辑${username}账户的权限`;
            res.status = 2;

        }

        // 当前登录用户, 有编辑此账户的权限
        else{

             // 不是超级管理员, 只有查询权限
            if ( !ctx.licence.isRoot( ctx.session.userInfo.username ) ){

                queryable = 1;
                addable   = 0;
                editable  = 0;
                deletable = 0;

            }

            // 更新某一个管理员用户资料
            const sql = `UPDATE admin SET password='${secret.sha1( password )}', status=${status},
                queryable=${queryable}, addable=${addable}, editable=${editable}, deletable=${deletable} WHERE username='${username}'`;

            const result = await db.do( sql );


            // 受影响的行数, 更新成功
            if( result.affectedRows > 0 ){

                // 响应的数据
                res.msg    = `管理员用户资料更新成功`;
                res.status = 200;

            }

            // 更新失败
            else{

                // 响应的数据
                res.msg    = `管理员用户资料更新失败, 此管理员已经不存在`;
                res.status = 0;

            }
        }

        ctx.body = res;

    })

    // 删除某一个管理员
    .get( '/deleteAdmin', async ctx => {
        const res = {};

        let { id, username } = ctx.query;

        id       = + id;
        username = username.trim();

        // 请求的参数是否合法
        if ( !id && !username ){

            res.status = 1;
            res.msg    = '非法参数'

        }

        // 当前登录用户, 没有删除此账户的权限
        else if ( !ctx.licence.delete( username ) ){

            res.msg    = '删除管理员失败, 此管理员禁止删除';
            res.status = 300;

        }

        // 当前登录用户, 有删除此账户的权限
        else{

            // 默认使用带 用户名 的参数请求方式
            let sql = `DELETE FROM admin WHERE username='${username}'`;

            // 如果请求参数有 id 的情况下, 使用带 id 的参数请求方式
            if ( id ){

                sql = `DELETE FROM admin WHERE id='${id}'`;

            }

            // 删除某一个管理员
            const result = await db.do( sql );

            // 受影响的行数, 删除成功
            if ( result.affectedRows > 0 ){

                // 响应的数据
                res.msg    = '删除管理员成功';
                res.data   = username ? username : id;  // 有用户名就返回用户, 否则返回 id
                res.status = 200;

            }

            // 删除失败
            else{

                // 响应的数据
                res.msg    = '删除管理员失败, 此管理员已经不存在';
                res.status = 0;

            }
        }

        ctx.body = res;

    });



module.exports = router;