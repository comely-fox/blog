/*
* @Author: comely-fox
* @Date:   2018-05-18 18:15:27
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-15 23:28:49
*/


const router = require( 'koa-router' )();
const db = require( '../../module/db.js' );
const secret = require( '../../module/secret.js' );


router
    .use( async ( ctx, next ) => {
        // 检测是否登录, 当前页不能为登录页,防止假死 , 不能为post且当前url不为登录检测行为
        if ( !ctx.session.sess_id && ctx.url != '/admin/user' && ctx.url != '/admin/user/login' ){ // 没有登录

            console.log( ctx.url )
            await ctx.redirect( '/admin/user' );
        }else{
            await next();
        }
    } )
    .get('/**')
    // 渲染后台首页
    .get( '/',  async ctx => {
        await ctx.render( 'admin/index.art' );
    } )



module.exports = router;