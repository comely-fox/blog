/*
* @Author: comely-fox
* @Date:   2018-05-17 16:16:41
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-05-19 11:31:42
*/

// 后台入口文件

const router = require( 'koa-router' )();


// 添加前缀
router.prefix( '/admin' );

// 后台首页
const index = require( './admin/index.js'  );

// 处理用户行为
const user = require( './admin/user.js' );

router.use( index.routes(), index.allowedMethods() );
router.use( user.routes(), user.allowedMethods() );



module.exports = router;

