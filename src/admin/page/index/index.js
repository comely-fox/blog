/*
* @Author: comely-fox
* @Date:   2018-05-12 16:23:21
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-25 19:15:21
*/

// 样式
import './index.scss' ;

// jquery 事件扩展
import 'util/jquery-resize.js';

// Loading
import Loading from 'page/common/loading/index.js';

// 页面头部
import Header  from 'page/common/header/index.js';

// 页面侧边导航栏
import Side    from 'page/common/side/index.js';

// 选项卡
import Tab     from 'util/tab/index.js';

// 选项卡
import Util    from 'util/blog.js';

class Index {

    constructor() {

        // 渲染loading
        Loading.render( $( 'body' ) );

        // 初始化头部
        Header.init();

        // 渲染侧边导航
        Side.render( $( '.side-nav' ) )

        // 渲染选项卡
        Tab.render( $( '.main-wrap' ) );

        // 计算内容区页面高度
        this.computeMainHeight();

        // 窗口被改变的时候重新获取
        this.winResize();

        // 所有密码可视
        Util.passView( '.input-veiw' );
    }

    // 计算内容区页面高度
    computeMainHeight( ){

        const $main = $( 'body main.main' );

        // 重置主要区的高度 防止误获取高度
        $main.height( 'auto' );
        const iBodyHeight     = $( 'body' ).outerHeight();
        const iHeaderHeight   = $( '.header' ).outerHeight();
        const iRealMainHeight = iBodyHeight - iHeaderHeight;

        // 取最高的时候页面的高度
        const iWHeight        = Math.max( $( document ).height() , $( window ).height() );
        const iWMainHeight    = iWHeight - iHeaderHeight;
        const iComputeHeight  = Math.max( iWMainHeight, iRealMainHeight );

        $main.height( iComputeHeight );
    }

    // 窗口被改变的时候重新获取
    winResize(){

        $( document ).resize( () => {

            this.computeMainHeight();
        } );

        $('.main-wrap').resize( () => {

            Tab.computeTabWidth( $( '.tab-tag' ) );
        } );
    }
}

$( function() {
    new Index();
} );

