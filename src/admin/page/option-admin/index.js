/*
* @Author: comely-fox
* @Date:   2018-05-25 15:36:35
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-27 19:08:34
*/


import './index.scss';
import templateAdd    from './add.string';
import templateList   from './list.string';
import templateUpdate from './update.string';

// Loading
import Loading    from 'page/common/loading/index.js';

// 引用格式化时间
import formatDate from 'util/formatDate.js';

// 引用公共方法库
import Util from 'util/blog.js';

// 引用提示窗口
import Tip from 'page/common/tip/index.js';
// 引用管理员用户接口
import UserServer from 'interface/user-server.js';
// 引用分页组件
import Pagination from 'util/pagination/index.js';

// 选项卡
import Tab from 'util/tab/index.js';

class OptionAdmin {

    constructor(){

        // 默认分页信息
        this.defaultListParems = {

            orderBy  : 'default',       // 默认以添加时间顺序排序
            pageNum  : 1,               // 默认第1页
            pageSize : 3               // 1页显示3条数据
        };

        this.$main      = $( '.main-wrap' );

        this.lastadd    = $();
        this.lastlist   = $();
        this.lastOption = { };
        this.addOption  = { };

        this.updateId   = new Set();

        this.index      = 0;
        this.option     = {};

        this.bindEvnet();
    }

    bindEvnet(){

        const that = this;

        // 绑定添加页输入控件的默认事件
        this.inputEvent( '.tab-add-admin .username', function( { username } ){

            // 查找此用户名是否可用
            that.findUsernameNotExist( username );
        });

        this.inputEvent( '.tab-add-admin', [ 'password', 'password2' ] );
        // END

        // 绑定更新页输入控件的默认事件
        this.inputEvent( '.tab-update-admin', [ 'password', 'password2' ] );
        // END

        // 重置添加管理员表单
        Util.resetInput( this.$main, '.tab-add-admin .btn-reset-admin', function( $parent ){

            // 用户名输入框获取焦点
            $parent.find( '.input.username' ).focus();

            // 重置表单为默认状态
            that.defaultFormStatus( $parent );
        });


        /**********************添加管理员 START*********************************/
        {
            this.$main.on( 'click', '.tab-add-admin .btn-add-admin', function(){

                const $parent   = $( this ).parents( '.tab-add-admin' );
                const username  = $parent.find( '.username' ).val().trim();
                const password  = $parent.find( '.password' ).val().trim();
                const password2 = $parent.find( '.password2' ).val().trim();

                // 权限检测
                // 默认为没有开启权限
                let [ queryable, addable, editable, deletable ] = Util.getCheckedValue( $( '.selected[type=checkbox]' ) );

                // 状态
                const status    = $( '.selected[type=radio]:checked' ).val() >> 0 > 0 ? 1 : 0;

                // 标题名称
                const title     = `添加管理员 - ${username}`;

                // 验证表单数据并提示
                that.dataValidateAndTip( $( '.tab-add-admin' ), { username, password, password2 } )
                    .then(

                        // 验证成功
                        ()=>{

                            // 查找此用户名是否可用
                            that.findUsernameNotExist( username ).then(

                                // 可以使用, 继续;
                                data => {

                                    // 执行添加管理员接口
                                    UserServer.addAdmin( { username, password, status, queryable, addable, editable, deletable } )
                                        .then(

                                            // 服务器返回添加成功
                                            data => {
                                                Tip({
                                                        statusCode: 200,
                                                        msg : '管理员账号添加成功',
                                                        title,
                                                        confirm() {

                                                        }
                                                    });
                                            },

                                            // 服务器返回添加失败
                                            ( [ err, status ] ) => {
                                                Tip({
                                                        statusCode: 0,
                                                        msg : err,
                                                        title,
                                                        close() {

                                                        }
                                                    });
                                            }
                                        );
                                }
                            );
                        }
                    )

            } );
        }
        /**********************添加管理员 END*********************************/

        /**********************更新管理员 STRAT*********************************/
        {


            this.$main.on( 'click', '.btn-edit-admin', function(){

                const $this    = $( this ),
                      id       = $( this ).data( 'id' ),
                      user     = $( this ).data( 'user' ),
                      disabled = $this.data( 'disabled' );

                // 编辑管理员资料按钮是禁用状态, 直接返回
                if ( disabled !== void 0 ){
                    return false;
                }

                // 权限检测, 是否是超级帐号, 且不能有编辑自己的权限
                const licence    = Util.licence( 'root' ) && !Util.isLoginUser( user );

                Loading.show();

                UserServer.getUserInfo( { id } )
                    .then(
                        data => {

                            Loading.hide();

                            that.loadedUpdateMessage(
                                Object.assign({},
                                    { id : id, licence },
                                    data
                                )
                            );
                        },
                        ( [ err, status ] ) => {
                            Tip( { statusCode : status, msg : err, title: '编辑管理员' } );
                        }
                    )

                return false;
            } );

            // 更新管理员界面 取消按钮
            this.$main.on( 'click', '.btn-update-cancel', function(){
                const id = $( this ).data( 'id' );

                Tab.matchId(

                 $( '.tab-update-admin' ), id, function(){
                    $( this ).fadeOut( 500 ).remove();
                    that.updateId.delete( id );
                    that.index--;
                } );
                return false;
            } );

            // 更新管理员界面 更新按钮
            this.$main.on( 'click', '.btn-update-admin', function(){
                const $parent            = $( this ).parents( '.tab-update-admin-wrap' ),
                      id                 = $parent.data( 'id' ),
                      username           = $parent.find( '.username' ).val(),
                      password           = $parent.find( '.password' ).val(),
                      password2          = $parent.find( '.password2' ).val(),
                      status             = +$parent.find('.selected[type=radio]:checked').val(),
                      title              = $parent.find( '.title' ).text().trim();

                // 权限检测
                // 默认为没有开启权限
                let [ queryable, addable, editable, deletable ] = Util.getCheckedValue( $( '.selected[type=checkbox]' ) );




                // 弹窗的参数
                const tipParams = Object.assign(
                        {
                            warpOffsetTop : $parent.offset().top,
                            wrapHeight    : $parent.outerHeight(),
                            title         : title
                        }
                    );

                that.dataValidateAndTip( $( `.tab-update-admin.for${id}` ), { password, password2 } )
                    .then(

                        () => {

                            // 更新某个管理员信息
                            UserServer.updateSomeUserInfo( { username, password, status, queryable, addable, editable, deletable } ).then(
                                data => {
                                    Tip( Object.assign(
                                        {},
                                        tipParams,
                                        {
                                            statusCode : 200,
                                            msg        : '管理员用户资料更新成功',
                                            confirm() {

                                                // 重新加载列表
                                                that.renderList( ( em ) => {
                                                    em.hide();
                                                } );

                                            }
                                        }
                                    ) );
                                },

                                ( [ err, status ] ) => {
                                    Tip( Object.assign( tipParams, { statusCode: 0, msg: err } ));

                                    //$('.btn-update-cancel').click();
                                }
                            );
                        }
                    );

                return false;
            } );
        }
        /**********************更新管理员 END*********************************/


        /**********************删除管理员 START*********************************/
        {
            this.$main.on( 'click', '.btn-delete-admin', function(){
                const $this    = $( this ),
                      id       = $this.data( 'id' ),
                      username = $this.data( 'user'),
                      disabled = $this.data( 'disabled' ),
                      title    = `删除管理员 - ${username}`;

                if ( disabled !== void 0 ){
                    return false;
                }

                Tip({
                    statusCode : 100,
                    msg        : `您确定要删除此管理员吗？`,
                    title,
                    confirm(){
                        UserServer.deleteAdmin( { username } ).then(
                            data => {
                                console.log( data )
                                Tip({
                                    statusCode : 200,
                                    msg        : `删除管理员${username}成功`,
                                    title,
                                    confirm() {
                                        // 重新加载列表
                                        that.renderList();
                                    }
                                });
                            },
                            ( [ err, status ] ) => {
                                Tip({
                                    statusCode : 0,
                                    msg        : err,
                                    title,
                                    close() {
                                        // 重新加载列表
                                        that.renderList();
                                    }
                                });
                            }
                        );
                    }
                });
            });
        }
        /**********************删除管理员 END*********************************/
    }

    /**
     * [inputEvent  输入控件时的一些检测方法与提示 ]
     * @param  { String   } selector [ 选择器, 可包含需要验证的控件类型, 需要同步一样的名称 ]
     * @param  { String   } type     [ 检测类型 ]
     * @param  { Function } cb       [ 验证通过的回调, 回调的参数返回的是要验证的数据 ]
     */
    inputEvent( selector, type, cb ){

        const that = this;

        if ( typeof type === 'function' ){

            [ type, cb ] = [ void 0, type ];
        }

        Util.bindDefaultEvent( this.$main, selector, type, {

            // 输入时
            onInput : this.dataValidateAndTip,

            // 获取焦点时
            onFocus : function( $parent, data, type, name ){

                // 重置表单为默认状态
                that.defaultFormStatus( ...arguments );

                // 添加获取焦点样式
                $( this ).parent().attr( 'class', 'input-box active' );
            },

            // 失去焦点时
            onBlur  : function( $parent, data, type, name ){

                // 移除获取焦点样式
                $( this ).parent().removeClass( 'active' );

            },

            // promise 成功
            resolve : cb
        })

    }

     /**
     * [defaultFormStatus 默认控件样式与提示信息]
     * @param  { Object } $parent [提示容器的父级 ]
     * @param  { String } name    [控件名     ]
     * @param  { String } type    [检测类型    ]
     */
    defaultFormStatus( $parent, data, type, name ){

        switch( name ){

            case void 0:

            case 'username':

                // 重置输入框为默认样式
                $parent.find( '.input.username' ).parent().attr( 'class', 'input-box' );

                // 重置为默认提示
                $parent.find( '.tip-text.username' ).attr( 'class', 'tip-text username' ).html('<i class="star-needful">*</i><b class="needful-text">必填</b>\
                        用户名长度为4-16位, 仅支持字母、数字、中间线和下划线');

                if ( name !== void 0 ){

                    break;
                }

            case 'password':

                // 重置输入框为默认样式
                $parent.find( '.input.password' ).parent().attr( 'class', 'input-box' );

                // 重置为默认提示
                $parent.find( '.tip-text.password' ).attr( 'class', 'tip-text password' ).html('<i class="star-needful">*</i><b class="needful-text">必填</b>\
                        密码长度为6-40位, 可以为字母、数字、特殊字符');

                if ( name !==void 0 ){

                    break;
                }

            case 'password2' :

                // 重置输入框为默认样式
                $parent.find( '.input.password2' ).parent().attr( 'class', 'input-box' );

                // 重置为默认提示
                $parent.find( '.tip-text.password2' ).attr( 'class', 'tip-text password2' ).html('<i class="star-needful">*</i><b class="needful-text">必填</b>\
                        密码同上');

        }
    }

    /**
     * [dataValidateAndTip  验证表单控件数据且提示 ]
     * @param  { Object        } data [ 提示的容器 ]
     * @param  { String | JSON } data [ 要验证的数据 ]
     * @param  { String        } type [ 检测类型 ]
     * @param  { String        } name [ 控件名 ]
     * @return { Object        }      [ Promise 对象]
     */
    dataValidateAndTip( $parent, data, type, name ){

        return new Promise( ( resolve, reject ) => {

            Util.formValidate( {

                data,
                type,
                full : true,

                // 验证过程中
                onProcess( name, value, checkType ){

                    // 需要密码强度提示
                    if ( checkType === 'password' ){

                        Util.gradeProgress( value.grade, $parent );
                    }

                },

                // 验证成功提示
                onSuccess( name, value ){

                    // 输入框父级成功样式
                    $parent.find( '.input.' + name ).parent().removeClass('active error').addClass( 'success' );

                    // 成功的提示样式与信息
                    $parent.find( '.tip-text.' + name ).removeClass( 'error' ).addClass( 'success' ).html( `<i class="fa fa-check-circle"></i>${value.text}` );
                },

                // 验证失败提示
                onError( name, value ){

                    // 输入框父级失败样式
                    $parent.find( '.input.' + name ).parent().removeClass('active success').addClass( 'error' );

                    // 失败的提示样式与信息
                    $parent.find( '.tip-text.' + name ).removeClass( 'success' ).addClass( 'error' ).html( `<i class="fa fa-times-circle"></i>${value.text}` );
                },

                // 所有验证都通过时执行
                allSuccess( data ){

                    resolve( data );
                }
            });

        });


    }


    /**
     * [ findUsernameExist 查找用户名是否可用 ]
     * @param  { String } username [ 用户名 ]
     * @return { Object }          [ 返回promise对象 ]
     */
    findUsernameNotExist( username ){
        return new Promise( ( resolve, reject ) => {
            UserServer.findUsernameExists( username )
                .then(

                    // 成功
                    data => {

                        $( '.tip-text.username' ).removeClass( 'error' ).addClass( 'success' ).html( `<i class="fa fa-check-circle"></i>用户名可用` );
                        resolve( data );
                    },

                    // 失败
                    ( [ err, status ] ) => {

                        // Tip( { statusCode: status, msg : err } , this.option.title );

                        $( '.tip-text.username' ).removeClass( 'success' ).addClass( 'error' ).html( `<i class="fa fa-times-circle"></i>${err}` );
                    }
                );
        } );
    }


    /**
     * [loadedHTML  根据标识加载不同的html]
     * @param  { String } identify [ 功能标识符 ]
     * @param  { JSON   } option   [ JSON数据信息 ]
     * @return { Object }          [ Promise对象 ]
     */
    loadedHTML( identify, option ){

        this.container = option.container;

        return new Promise( ( resolve, reject ) => {

            switch( identify ){

                // 检测是显示所有管理员成员
                case 'list' :

                    this.listOption = option;

                    if ( Util.licence( 'query' ) ){

                        this.renderList( $content => {

                            resolve( $content );
                        });

                    }else{

                        reject( [ '没有查看管理员列表的权限', 0 ] );
                    }

                    break;

                // 检测是要添加管理员
                case 'add' :

                    this.addOption = option;

                    if ( Util.licence( 'add' ) ){

                        this.renderAdd( );

                        resolve( this.lastadd );

                    }else{

                        reject( [ '没有添加管理员账户的权限', 0 ]  );

                    }

            }

        } );

    }

    /**
     * [renderAdd  渲染添加管理员页面 ]
     * @return { Object }  [ 添加管理员元素容器 ]
     */
    renderAdd( ){

        if ( !this.renderAdd.render ){

            // 渲染列表数据html
            const html = Util.render( templateAdd, Object.assign( {}, this.addOption ) );

            // 保存当前渲染的html对象
            this.lastadd = $( html ).eq( 0 );

            // 把html放入页面中
            this.addOption.container.append( this.lastadd );

            this.renderAdd.render = true;
        }

        else{

            this.lastadd.show();

        }

        return this.lastadd;

    }

    /**
     * [renderList 渲染管理员列表页面]
     * @param  { Function } cb [ 成功后的回调函数 ]
     * @return { Object   }    [ 列表管理员元素容器 ]
     */
    renderList( cb ){

        Loading.show();

        UserServer.getAdminList( this.defaultListParems ).then(

            data => {

                this.listOption.container.find('.loading-effect').hide();

                // 格式化时间戳
                data.list.forEach( ( item ) => {

                    if ( item.add_time ){

                        item.add_time   = formatDate( item.add_time );
                    }

                    if ( item.login_time ){

                        item.login_time = formatDate( item.login_time );
                    }

                });

                // 移除上一次渲染过的内容
                this.lastlist.remove( );

                // 渲染列表数据html
                const html = Util.render( templateList, Object.assign( {}, this.listOption, { list : data.list } ) );

                // 保存当前渲染的html对象
                this.lastlist = $( html ).eq( 0 );

                // 把html放入页面中
                this.listOption.container.append( this.lastlist );

                // 调用加载分页方法
                this.loadingPagination(
                    {
                        hasPreviousPage : data.hasPreviousPage,  // 是否有上一页
                        prevPage        : data.prevPage,         // 上一页是几
                        hasNextPage     : data.hasNextPage,      // 是否有下一页
                        nextPage        : data.nextPage,         // 下一页是几
                        pageNum         : data.pageNum,         // 当前页码
                        pages           : data.pages             // 总页数
                    }
                );

                typeof cb === 'function' && cb.call( this.lastlist, this.lastlist );

                Loading.hide();

            },
            ( [ err, status ] ) => {

                console.log( err, status )
            }
        );

        return this.lastlist;
    }

    /**
     * [loadedUpdateMessage  加载更新资料html ]
     * @param  { JSON } jsonData [  ]
     * @return {[type]}          [description]
     */
    loadedUpdateMessage( jsonData ){
        const jsonData2  = Object.assign( {}, { render: this.renderUpdate, index: this.index++ }, jsonData );
        const id         = jsonData2.id;
        const html       = Util.render( templateUpdate, jsonData2 );
        const $updateObj = $( html );

        // END
        if ( !this.renderUpdate ){

            Tab.addTab( $updateObj, '修改管理员' );
            // 置选项卡内容区全部隐藏
            $( '.tab-content' ).hide();
            this.container.append( $updateObj );
            this.renderUpdate = $updateObj;

            this.updateId.add( id );

        }else{

            Tab.toggleTab( Tab.getId( this.renderUpdate ) );
            if ( !this.updateId.has( id ) ){
                this.renderUpdate.append( $updateObj );

                this.updateId.add( id );
            }
            Tab.matchId( $( '.tab-update-admin' ), id, function(){
                const index   = this.data( 'index' );
                const $height = this.outerHeight( true );
                const $top    = index * $height;
                $( 'html,body' ).animate({
                       scrollTop: $top
                    },
                    100
                );
            } );
        }
    }


    /**
     * [loadingPagination 加载分页信息 ]
     * @param  { JSON } optionInfo [ 分页的参数 ]
     */
    loadingPagination( optionInfo ){

        const paginationHTML = Pagination.render(
            optionInfo,
            ( num ) => {

                // 设置要查询的页码
                this.defaultListParems.pageNum = num;

                // 加载列表数据与html
                this.renderList( );
            }
        );

        // 追加到分布容器中
        this.lastlist.find( '.pagination-wrap' ).html( paginationHTML );
    }
}


export default new OptionAdmin();