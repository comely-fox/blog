/*
* @Author: comely-fox
* @Date:   2018-05-19 11:05:22
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-27 19:29:09
*/

import './index.scss';

import UserServer   from 'interface/user-server.js';
import Util         from 'util/blog.js';

import Tip          from 'page/common/tip/index.js';

import Tab          from 'util/tab/index.js';

// 引用格式化时间
import formatDate   from 'util/formatDate.js';

import templateUser from './user.string';

class Header {

    init() {

        this.$userMenuElems = null;
        this.data           = null
        this.bindEvent();
        this.loadingUserInfo();
    }

    bindEvent(){

        const that      = this;
        const $userMenu = $( '.user-menu' );

        // 用户导航菜单
        $( '.userinfo-bar' ).hover(

            function() {

                $userMenu.stop().fadeIn();
                $( this ).addClass( 'active' );
            },

            function() {

                $userMenu.stop().fadeOut();
                $( this ).removeClass( 'active' );
            }
        );


        // 注销登录
        $userMenu.find( '.logout' ).click( function(){

            UserServer.logout().then(

                data => {

                    // 回到登录首页
                    Util.goLogin();
                }
            )

            return false;
        } );

        // 用户下拉菜单单击事件
        $userMenu.on( 'click', '.user', function(){

            // 菜单绑定的id
            const id = $( this ).data( 'id' );

            // 只渲染一次用户相关操作html
            if ( !Util.getData( $userMenu, 'render' ) ){

                const html = Util.render( templateUser, that.data );

                that.$upwardPage = $( html ).eq( 0 );

                // 添加一个专有的class名, 不与其它项冲突
                that.$upwardPage.addClass( 'up-header' );

                // 追加到body内容的最后
                $( 'body' ).append( that.$upwardPage );

                // 置为渲染过
                Util.setData( $userMenu, 'render', 1 );
            }

            // 动画并显示
            that.$upwardPage.stop().fadeIn( 200 );

            const $navItem     = that.$upwardPage.find( '.nav-item' ),
                  $contentItem = that.$upwardPage.find( '.content-item' );

            // 根据id切换对象的导航与内容
            Tab.toggleTab( id, $navItem, $contentItem );

            return false;
        });

        // 用户下拉菜单弹窗-> 菜单切换
        $( document ).on( 'click', '.upward-page.up-header .nav-item', function(){

            const id           = $( this ).data( 'id' ),
                  $navItem     = $( '.upward-page.up-header .nav-item' ),
                  $contentItem = $( '.upward-page.up-header .content-item' );

            // 根据id切换对象的导航与内容
            Tab.toggleTab( id, $navItem, $contentItem );
        });

        // 关闭用户相关操作窗口
        $( document ).on( 'click', '.upward-page.up-header .close', function(){

            $('.upward-page.up-header').stop().fadeOut( );
        });

         // 更新管理员密码-> 重置按钮单击事件
        Util.resetInput( '.upward-page.up-header .update-pass .btn-reset', function( $parent ){

            $parent.find( 'input[name=username]' ).val( that.data.username );

            that.defaultFormStatus( $parent );
        });

        // 绑定默认输入事件
        // 原始密码输入框
        this.inputEvent( '.upward-page.up-header .item .oldPassword', 'password' );

        // 绑定默认输入事件
        // 新密码输入框
        this.inputEvent( '.upward-page.up-header .item .newPassword', 'password' );

        // 绑定默认输入事件
        // 确认新密码输入框
        this.inputEvent( '.upward-page.up-header .item .newPassword2', 'password2' );

        // 更新管理员密码-> 确认按钮单击事件
        $( document ).on( 'click', '.upward-page.up-header .update-pass .btn-confirm', function( ev ){

            const $parent      = $( this ).parents( '.update-pass' );
            const oldPassword  = $parent.find( '.oldPassword' ).val().trim();
            const newPassword  = $parent.find( '.newPassword' ).val().trim();
            const newPassword2 = $parent.find( '.newPassword2' ).val().trim();

            // 验证表单数据并提示
            that.dataValidateAndTip(

                // parent
                $parent,

                // data
                { oldPassword, newPassword, newPassword2 },

                // checkType
                [ 'password', 'password', 'password2' ]
            )
            .then(

                // 验所有数据证成功
                data => {

                    const tipTitle = '修改密码';

                    // ajax请求, 更新管理员密码
                    UserServer.updateAdminPass( { oldPassword, newPassword, newPassword2 } )
                    .then(
                        data => {
                            Tip({
                                statusCode : 200,
                                msg        : '密码更新成功',
                                title      : tipTitle,
                                confirm(){

                                    UserServer.logout().then( data => {
                                        // 回到登录首页
                                        Util.goLogin();
                                    } );
                                }
                            });
                        },
                        ( [ err, status ] ) => {
                            Tip({
                                statusCode : 0,
                                msg        : err,
                                title      : tipTitle
                            });
                        }
                    );

                }

            );

        });

    }

    /**
     * [inputEvent  输入控件时的一些检测方法与提示 ]
     * @param  { String   } selector [ 选择器, 可包含需要验证的控件类型, 需要同步一样的名称 ]
     * @param  { String   } type     [ 检测类型 ]
     * @param  { Function } cb       [ 验证通过的回调, 回调的参数返回的是要验证的数据 ]
     */
    inputEvent( selector, type, cb ){

        const that = this;

        if ( typeof type === 'function' ){

            [ type, cb ] = [ void 0, type ];
        }

        Util.bindDefaultEvent( selector, type, {

            // 输入时
            onInput : this.dataValidateAndTip.bind( this ),

            // 获取焦点时
            onFocus : function( $parent, data, type, name ){

                // 重置表单为默认状态
                that.defaultFormStatus( ...arguments );

                // 添加获取焦点样式
                $( this ).parent().attr( 'class', 'input-box active' );
            },

            // 失去焦点时
            onBlur  : function( $parent, data, type, name ){

                // 移除获取焦点样式
                $( this ).parent().removeClass( 'active' );
            },
            resolve : cb
        })

    }

    /**
     * [defaultFormStatus 默认表单控件提示]
     * @param  { Object } $parent [ 提示容器的父级 ]
     * @param  { String } name    [ 控件名 ]
     * @param  { String } type    [ 检测类型 ]
     */
    defaultFormStatus( $parent, data, type, name ){

        // 重置输入框为默认样式
        $parent.find( '.input-box' ).attr( 'class', 'input-box' );

        // 隐藏提示框
        $parent.find( '.tip-box' ).hide();
    }

    /**
     * [dataValidateAndTip  验证表单控件数据且提示 ]
     * @param  { Object        } data [ 提示容器的父级 ]
     * @param  { String | JSON } data [ 要验证的数据 ]
     * @param  { String        } type [ 检测类型 ]
     * @param  { String        } name [ 控件名 ]
     * @return { Object        }      [ Promise 对象]
     */
    dataValidateAndTip( $parent, data, type, name ){

        $parent.find( '.tip-box' ).show();

        return new Promise( ( resolve, reject ) => {

            Util.formValidate( {

                data,
                type,
                full : true,

                // 验证过程中
                onProcess( name, value, checkType ){

                    // 需要密码强度提示
                    if ( checkType === 'password' && name !== 'oldPassword' ){

                        Util.gradeProgress( value.grade, $parent );
                    }

                },

                // 验证成功提示
                onSuccess( name, value ){

                    // 输入框父级成功样式
                    $parent.find( '.input.' + name ).parent().removeClass('active error').addClass( 'success' );

                    // 成功的提示样式与信息
                    $parent.find( '.tip-text.' + name ).removeClass( 'error' ).addClass( 'success' ).html( `<i class="fa fa-check-circle"></i>${value.text}` );
                },

                // 验证失败提示
                onError( name, value ){

                    // 输入框父级失败样式
                    $parent.find( '.input.' + name ).parent().removeClass('active success').addClass( 'error' );

                    // 失败的提示样式与信息
                    $parent.find( '.tip-text.' + name ).removeClass( 'success' ).addClass( 'error' ).html( `<i class="fa fa-times-circle"></i>${value.text}` );
                },

                // 所有验证都通过时执行
                allSuccess( data ){

                    resolve( data );
                }
            });

        });
    }

    // 加载当前登录用户的信息
    loadingUserInfo() {

        UserServer.checkLogin()
            .then(
                data => {

                    $( '.user-title' ).text( data.username );

                    // 格式化时间戳
                    data.add_time   = formatDate( data.add_time );

                    data.login_time = formatDate( data.login_time );

                    this.data = data;
                },

                ( [ error, msg ]  ) =>{

                }
            );
    }

};

export default new Header();