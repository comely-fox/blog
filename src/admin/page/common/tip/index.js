/*
* @Author: comely-fox
* @Date:   2018-05-23 10:10:32
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-23 23:20:36
*/

import './index.scss';
import 'util/jquery-Tdrag.js';


class Tip {

    constructor() {

        this.fnConfirm = function(){ };
        this.fnClose   = function(){ };

        this.bindEvent();
    }

    bindEvent(){
        const that = this;

        // 提示窗口的关闭按钮, 或者确认按钮被单击的时候
        $( '.module-tip .close,.module-tip .confirm' ).click( function() {

            // 隐藏遮罩和提示窗口
            $( '.module-mask' ).hide();
            $( '.module-tip').hide();

            // 确认按钮点击时需要执行的回调
            if ( $( this ).hasClass( 'confirm' ) ){
                that.fnConfirm();
                that.fnConfirm = function(){};
            }

            // 关闭按钮点击时需要执行的回调
            if ( $( this ).hasClass( 'close' ) ){

                that.fnClose();
                that.fnClose = function(){};
            }

            // 重置所有样式为默认状态
            $( '.module-tip .fa' ).attr('class', 'fa');
            $( '.module-tip .msg' ).attr( 'class', 'msg' );
            $( '.module-tip .confirm' ).removeClass( 'delete' );

            $( '.module-tip .close,.module-tip .confirm' ).hide();

        } );
    }

    /**
     * [ tip 窗口提示 ]
     * @param  { json     }   json  [ json.statusCode 状态码 [ 100, 200, 300, 其它 ], json.msg 提示信息 ]
     * @param  { Function }   fn    [ 回调函数 ]
     */
    tip( json ){
        // 显示窗口的时候, 绑定回调函数

        if ( typeof json.confirm === 'function' ){

            this.fnConfirm = json.confirm;
        }
        if ( typeof json.close === 'function' ){

            this.fnClose = json.close;
        }

        const $tip = $( '.module-tip' );

        $( '.module-mask' ).show();

        $tip.show( );

        $tip.find( '.title' ).text( json.title );

        $tip.find( '.msg' ).text( json.msg );

        $tip.css( {

         'transform' : 'translate3d(0,0,0)',
         'left'      : $( window ).width() / 2 - $tip.outerWidth() / 2,
         'top'       : json.wrapHeight / 2 - $tip.outerHeight() / 2 + json.warpOffsetTop || ( $( window ).height() - $tip.outerHeight() ) / 2
        });

        $tip.Tdrag( );

        switch( Number( json.statusCode ) ){

            // 成功
            case 200:

                $( '.module-tip .fa' ).addClass( 'fa-check' );
                $( '.module-tip .msg' ).addClass( 'success' );
                $( '.module-tip .confirm' ).show();

                break;

            // 疑问
            case 100:

                $( '.module-tip .fa' ).addClass( 'fa-question' );
                $( '.module-tip .msg' ).addClass( 'query' );
                $( '.module-tip .close' ).show();
                $( '.module-tip .confirm' ).show().addClass( 'delete' );

                break;

            // 提示
            case 300:

                $( '.module-tip .fa' ).addClass( 'fa-info' );
                $( '.module-tip .msg' ).addClass( 'info' );
                $( '.module-tip .close' ).show();

                break;

            // 其它情况, 如 错误
            default :

                $( '.module-tip .fa' ).addClass( 'fa-close' );
                $( '.module-tip .msg' ).addClass( 'error' );
                $( '.module-tip .close' ).show();
        }

    }

}
const obj = new Tip();

export default obj.tip.bind( obj );