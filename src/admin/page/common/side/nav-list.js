/*
* @Author: comely-fox
* @Date:   2018-05-25 17:38:40
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-14 16:37:11
*/

import Util from 'util/blog.js';

export default {
    list : [
        {
            title  : '管理员管理',
            style  : 'fa-user-o',
            option : 'admin',
            subNav : [
                {
                    style    : 'fa-bars',
                    forbid   : !Util.licence( 'query' ),
                    title    : '管理员列表',
                    identify : 'list'
                },
                {
                    style    : 'fa-plus',
                    title    : '添加管理员',
                    forbid   : !Util.licence( 'add' ),
                    identify : 'add'
                }
            ]
        },
        {
            title  : '轮播图管理',
            style  : 'fa-image',
            option : 'banner',
            subNav : [
                {
                    style    : 'fa-bars',
                    title    : '轮播图列表',
                    identify : 'list'
                },
                {
                    style    : 'fa-plus',
                    title    : '添加轮播图',
                    identify : 'add'
                }
            ]
        },
        {
            title  : '分类管理',
            style  : 'fa-align-justify',
            option : 'category',
            subNav : [
                {
                    style    : 'fa-bars',
                    title    : '分类列表',
                    identify : 'list'
                },
                {
                    style    : 'fa-plus',
                    title    : '添加分类',
                    identify : 'add'
                }
            ]
        },
        {
            title  : '内容管理',
            style  : 'fa-edit',
            option : 'content',
            subNav: [
                {
                    style    : 'fa-bars',
                    title    : '内容列表',
                    identify : 'list'
                },
                {
                    style    : 'fa-plus',
                    title    : '添加内容',
                    identify : 'add'
                }
            ]
        },
        {
            title  : '导航管理',
            style  : 'fa-flag-checkered',
            option : 'nav',
            subNav: [
                {
                    style    : 'fa-bars',
                    title    : '导航列表',
                    identify : 'list'
                },
                {
                    style    : 'fa-plus',
                    title    : '添加导航',
                    identify : 'add'
                }
            ]
        },
        {
            title  : '友情链接管理',
            style  : 'fa-link',
            option : 'link',
            subNav: [
                {
                    style    : 'fa-bars',
                    title    : '友情链接列表',
                    identify : 'list'
                },
                {
                    style    : 'fa-plus',
                    title    : '添加友情链接',
                    identify : 'add'
                }
            ]
        }
    ]
}