/*
* @Author: comely-fox
* @Date:   2018-05-19 15:08:23
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-25 17:41:06
*/


import './index.scss';

import sideTemplate from './index.string';

import Util         from 'util/blog.js';

// 引用导航菜单选项内容区

import OptionAdmin  from 'page/option-admin/index.js';

// 引用导航菜单数据
import navList      from './nav-list.js';

import Tab          from 'util/tab/index.js';

// 引用提示窗口
import Tip          from 'page/common/tip/index.js';

class Side {

    // 渲染侧边导航
    render( $container ){

        const result = Util.render( sideTemplate, navList );

        $container.append( result );

        this.bindEvnet();
    }

    // 绑定事件
    bindEvnet(){

        const that = this;

        const $sideNav = $( '.side-nav' );

        const _switch = Util.switch(

                () => {

                    $sideNav.css( { 'marginLeft' : -$( '.side-nav' ).outerWidth( ) });
                },

                () => {

                    $sideNav.css( { 'marginLeft' : 0 } );
                }
        );

        // 显示、隐藏左侧栏
        $sideNav.find( '.side-nav-switch' ).click( function(){

            const $this = $( this );

            $this.hide();

            $this.siblings( ).show();

            _switch();

        });


        // 列表栏标题被单击
        $sideNav.find( '.item .title' ).click( function(){

            const $this = $( this );

            $this.toggleClass( 'active' );

            // 查询子菜单
            const $itemMenu = $this.siblings( '.item-menu' );

            $itemMenu.toggle( 300 );

            // 没有子菜单
            if ( $itemMenu.length === 0 ){

                Tab.addTab( $this, $this.text().trim() );
            }

        });

        $sideNav.find( '.item .list-subNav' ).click( function( ev ){

            const $this     = $( this );

            // 获取列表分类标识
            const sOption   = $this.parents( '.item-menu' ).data( 'option' );

            // 当前分类下的操作
            const identify  = $this.data( 'identify' );

            // 当前分类下的标题
            const title     = $this.data( 'title' );

            // 右侧内容容器
            const $wrap     = $( '.main-wrap .module-tab-content' );


            // 是否是超级管理员
            const isRoot    = Util.licence( 'root' );

            // 添加选项卡
            Tab.addTab( $this, title, function( id, render ){

                switch( sOption ){

                    case 'admin' :

                        OptionAdmin.loadedHTML( identify, { title, id, isRoot, container: $wrap } )
                            .then(

                                // 有权限, 渲染内容成功
                                $content => {

                                    // 置选项卡内容区全部隐藏
                                    $( '.tab-content' ).hide();

                                    $content.show();

                                    // 渲染选项卡
                                    render( true );

                                },

                                // 没有权限, 渲染内容失败
                                ( [ err, status ] ) => {


                                    Tip( { statusCode: 0, msg: err, title } );

                                    // 不渲染选项卡
                                    render( false );
                                }

                            );

                        break;
                }

            });


        });
    }

}

export default new Side();