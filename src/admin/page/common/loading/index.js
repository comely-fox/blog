/*
* @Author: comely-fox
* @Date:   2018-06-25 18:12:37
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-25 18:58:58
*
*
* 用于页面的加载共用同一个loading
*/

import './index.scss';

import templateLoading from './index.string';

// 引用公共方法库
import Util from 'util/blog.js';

class Loading {

   // 渲染loading图标
    render( $container ){

        this.container = $container;

        const result = Util.render( templateLoading );

        $container.append( result );

    }

    // loading弹出窗显示
    show(){

        this.container.find( '.upward-page.loading-page' ).show();
    }

    // loading弹出窗隐藏
    hide(){

        this.container.find( '.upward-page.loading-page' ).hide();
    }

}

export default new Loading();