/*
* @Author: comely-fox
* @Date:   2018-05-13 15:37:51
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-27 19:02:24
*/

require( './index.scss' );

import Tip        from 'page/common/tip/index.js';

import Util       from 'util/blog.js';

import UserServer from 'interface/user-server.js';


class Login {

    constructor() {

        this.$main = $( '.login-wrap' );

        this.bindEvent();
    }

    // 事件绑定
    bindEvent(){

        const that = this;

        // 所有密码可视
        Util.passView( $( document ), '.input-veiw' );

        // 绑定输入框的默认事件
        this.inputEvent( '.input-login', [ 'username', 'password' ] );

        // 登录操作
        {
            const e = jQuery.Event( 'click' );

            // 回车登录
            this.$main.keydown( ( ev ) => {

                if ( ev.keyCode === 13 ){

                    // 触发单击登录
                    $( '.btn-login' ).trigger( e );
                }
            });

            // 单击登录
            $( '.btn-login' ).click( function(){

                const keys     = [ 'username', 'password' ];
                const $parent  = $( '.login-wrap' );

                const username = $parent.find( '.username' ).val().trim();
                const password = $parent.find( '.password' ).val().trim();

                // 隐藏眼睛
                $parent.find( '.input-veiw' ).hide();

                that.dataValidateAndTip( $parent, { username, password }, keys )
                    .then(

                        data => {

                            $( '.btn-login .btn-text' ).text( '登录中...' );

                            UserServer.login( data ).then(

                                // 登录成功
                                data => {

                                    // 设置cookie 用户信息
                                    Util.cookies( data );

                                    // 回管理首页
                                    Util.goAdmin();

                                },

                                // 登录失败
                                ( [ err, status ] ) => {

                                    // 没有权限
                                    if ( status === 2 ){

                                        Tip( {
                                            msg        : err,
                                            statusCode : 0,
                                            title      : '管理员登录',
                                            close(){

                                                $( '.btn-login .btn-text' ).text( '登录' );
                                            }
                                        });
                                    }

                                    // 有权限, 可能账户、密码错误
                                    else {

                                        const type = keys[ status ];

                                        that.inlineTip( $parent.find( '.' + type ).parent(), type, err )[ 'reject' ]( );

                                        $( '.btn-login' ).text( '登录' );

                                    }
                                }

                            );

                        }
                    )
            });

        }

    }

    /**
     * [inputEvent  输入控件时的一些检测方法与提示 ]
     * @param  { String   } selector [ 选择器, 可包含需要验证的控件类型, 需要同步一样的名称 ]
     * @param  { String   } type     [ 检测类型 ]
     * @param  { Function } cb       [ 验证通过的回调, 回调的参数返回的是要验证的数据 ]
     */
    inputEvent( selector, type, cb ){

        const that = this;
        if ( typeof type === 'function' ){

            [ type, cb ] = [ void 0, type ];
        }

        Util.bindDefaultEvent( this.$main, selector, type, {

            // 输入时
            onInput : this.dataValidateAndTip.bind( that ),

            // 获取焦点时
            onFocus : this.defaultFormTip.bind( that ),

            // 失去焦点时
            onBlur : function( $parent, data, type, name ){

                // 移除获取焦点样式
                $parent.removeClass( 'active' );
            },

            // promise 成功
            resolve : cb
        });
    }

    /**
     * [defaultFormTip 默认表单控件提示]
     * @param  { Object } $parent [提示容器的父级 ]
     * @param  { String } name    [控件名     ]
     * @param  { String } type    [检测类型    ]
     */
    defaultFormTip( $parent, data, type, name ){

        // 眼睛显示
        $parent.find( '.input-veiw' ).show( )

        // // 添加获取焦点样式
        $parent.attr( 'class', 'input-box input-login active' );

        // 重置提示样式与信息为空
        $parent.find( '.tip-text' ).attr( 'class', 'tip-text ' +  name ).text( '' );
    }

    /**
     * [dataValidateAndTip  验证表单控件数据且提示 ]
     * @param  { Object        } data [ 提示容器的父级 ]
     * @param  { String | JSON } data [ 要验证的数据 ]
     * @param  { String        } type [ 检测类型 ]
     * @param  { String        } name [ 控件名 ]
     * @return { Object        }      [ Promise 对象]
     */
    dataValidateAndTip( $parent, data, type, name ){

        const that = this;

        return new Promise( ( resolve, reject ) => {

            Util.formValidate( {

                data,
                type,

                // 验证成功提示
                onSuccess( name, value ){

                    that.inlineTip( $parent, name, value.text )[ 'resolve' ]( );
                },

                // 验证失败提示
                onError( name, value ){

                    that.inlineTip( $parent, name, value.text )[ 'reject' ]( );
                },

                allSuccess( data ){

                    resolve( data );
                }
            });

        });
    }

    /**
     * [inlineTip 内部提示 ]
     * @param  { Object } $parent    [ 提示的容器  ]
     * @param  { String } name       [ 控件名  ]
     * @param  { String } msg        [ 提示信息  ]
     */
    inlineTip( $parent, name, msg ){

        // 提示容器
        const $tipText = $parent.find( '.tip-text.' + name );

        return {

            // 验证成功提示
            'resolve' : function( ){

                $tipText.parent().removeClass( 'error active' ).addClass( 'success' );

                $tipText.removeClass( 'fa-exclamation-circle' ).addClass( 'fa fa-check' ).text( '' );
            },

            // 验证失败提示
            'reject' : function( ){

                $tipText.parent().removeClass( 'success active' ).addClass( 'error' );
                $tipText.removeClass( 'fa-check' ).addClass( 'fa fa-exclamation-circle' ).text( msg );
            }

        }
    }

}

$( function(){

    $( 'body' ).css( {

        height: Math.max( $( window ).height(), $( 'body' ).height() )
    });

    new Login();
});
