/*
* @Author: comely-fox
* @Date:   2018-05-13 18:22:12
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-15 17:43:31
*/

import Util from 'util/blog.js';
export default class UserServer {

    /**
     * [ login 登录验证 ]
     * @param  { Json } userInfo   [ 用户信息 ]
     * @return { Object }          [ Promise ]
     */
    static login( userInfo ){
        return Util.request({
            url     : Util.getServerUrl( '/admin/user/login' ),
            data    : userInfo,
            type    : 'post'
        });
    }

    /**
     * [checkLogin 检查登录]
     */
    static checkLogin( ){
        return Util.request({
            url     : Util.getServerUrl( '/admin/user/checkLogin' )
        });
    }

    /**
     * [logout 退出登录]
     * @return { Object }          [ Promise ]
     */
    static logout( ){
        return Util.request({
            url     : Util.getServerUrl( '/admin/user/logout' )
        });
    }
    /**
     * [ getAdminList 获取所有管理员账户 ]
     * @param  { Json } listParam  [ json数据 ]
     * @return { Object }          [ Promise ]
     */
    static getAdminList( listParam ){
        return Util.request({
            url     : Util.getServerUrl( '/admin/user/list' ),
            data    : listParam
        });
    }

    /**
     * [ addAdmin 添加管理员账户 ]
     * @param  { Json   } userInfo [ 用户信息 ]
     * @return { Object }          [ Promise ]
     */
    static addAdmin( userInfo ){
        return Util.request({
            url     : Util.getServerUrl( '/admin/user/add' ),
            data    : userInfo,
            type    : 'post'
        });
    }

    /**
     * [ findUsername 查找某个账户是否存在 ]
     * @param  { String } username [ 管理员账户名 ]
     * @return { Object }          [ Promise ]
     */
    static findUsernameExists( username ){
        return Util.request({
            url     : Util.getServerUrl( '/admin/user/findUsernameExists' ),
            data    : {
                username : username
            },
            type : 'post'
        });
    }

    /**
     * [ getUserInfo 获取用户信息 ]
     * @param  { JSON   } id, username [ id, 或者用户名 ]
     * @return { Object }                        [ Promise ]
     */
    static getUserInfo( userInfo ){
        return Util.request({
            url     : Util.getServerUrl( '/admin/user/getUserInfo' ),
            data    : {
                id       : userInfo.id       || 0,
                username : userInfo.username || ''
            }
        });
    }

    /**
     * [updateAdminPass 用户登录状态下更新密码]
     * @param  { JSON   } userInfo   [ 旧密码, 新密码 ]
     * @return { Object }          [ Promise ]
     */
    static updateAdminPass( userInfo ) {
        return Util.request({
            url     : Util.getServerUrl( '/admin/user/updateAdminPass' ),
            data    : userInfo,
            type    : 'post'
        });
    }

    /**
     * [updateUserPass 更新某一个管理员用户资料]
     * @param  { JSON   } userInfo [ 用户名, 旧密码, 新密码, 状态, ... ]
     * @return { Object }          [ Promise ]
     */
    static updateSomeUserInfo( userInfo ) {
        return Util.request({
            url     : Util.getServerUrl( '/admin/user/updateSomeUserInfo' ),
            data    : userInfo,
            type    : 'post'
        });
    }

   /**
     * [ deleteAdmin 删除管理员 ]
     * @param  { JSON   } id, username [ id, 或者用户名 ]
     * @return { Object }                        [ Promise ]
     */
    static deleteAdmin( userInfo ){
        return Util.request({
            url : Util.getServerUrl( '/admin/user/deleteAdmin' ),
            data : {
                id       : userInfo.id       || 0,
                username : userInfo.username || ''
            }
        });
    }
}
