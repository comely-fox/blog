/*
* @Author: comely-fox
* @Date:   2018-05-22 11:06:59
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-05-22 11:43:58
*/


export default function( timestamp ){
    const objDate = new Date( timestamp );
    const year = objDate.getFullYear();
    const month = objDate.getMonth() + 1;
    const day = objDate.getDate();
    const hours = objDate.getHours();
    const minutes = objDate.getMinutes();

    return `${year}-${month}-${day} ${hours}:${minutes}`;
}