/*
* @Author: comely-fox
* @Date:   2018-05-26 15:13:49
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-05-27 11:25:11
*/

import './index.scss';

import template from './index.string';

import Util from 'util/blog.js';


class Pagination {

    constructor() {

        // 默认参数, 可修改
        this.defaultParams = {
            container    : null,  // 容器优先级高
            pageNum      : 1,     // 优先级低。 调用时没有给值就使用这个
            pageRange    : 4,
            onSelectPage : null
        }

        this.bindEvnet();

    }

    bindEvnet() {

        const that = this;

        $( document ).on( 'click', '.p-item', function() {

            const $this = $( this );
            that.defaultParams.pageNum = $this.data( 'num' );
            if ( $this.hasClass( 'disabled' ) || $this.hasClass( 'active' ) ){
                return;
            }

            typeof that.callback === 'function' && that.callback( $this.data( 'num' ) );

        } );

    }

    render( option, callback ) {

        this.options = Object.assign( {}, this.defaultParams, option );

        this.callback = callback;

        // 容器不是一个jQuery对象实例直接返回
        if ( this.options.container && !( this.options.container instanceof jQuery ) ){
            return ;
        }

        // 总页数等于或小于1的时候不进行渲染
        if ( this.options.pages <= 1 ){

            return;
        }


        return this.getPaginationHtml();
    }

    getPaginationHtml() {
        const data = {
            list    : [],
            pageNum : this.options.pageNum,
            pages   : this.options.pages
        };

        const options = this.options;

        // 奇数 取下
        let skip = Math.floor( options.pageRange / 2 );

        let start = options.pageNum - skip > 0
                ? options.pageNum - skip
                : 1;

        // 出现范围是偶数的时候让断点减1, 保证范围 = 当前页码 + 前间距 ＋ 后间距
        if ( skip * 2 === options.pageRange ){
            skip--;
        }
        let end = options.pageNum + skip < options.pages
                ? options.pageNum + skip
                : options.pages;

        // 上一页按钮
        data.list.push(
            {
                btn      : true,
                title    : '<上一页',
                value    : this.options.prevPage,
                disabled : !this.options.hasPreviousPage
            }
        );

        // 允许出现断点的时候 比如  页码范围是4   开始页码是3的时候 skip 是1 + 1 就小于 开始页码 出现断点
        if ( start > skip + 1 ){
            // 第一页
            data.list.push(
                {
                    title    : 1,
                    value    : 1
                }
            );
            // 断点
            data.list.push(
                {
                    break    : true
                }
            );
        }

        // 容错, 开始页码 === skip + 1 开始页码往后退一步 与prev断点配合统一
        if ( start === skip + 1 ){
            start--;
        }

        // 容错, 结束页码 === 总页数 - 1 结束页码往前进一步 与next断点配合统一
        if ( end === options.pages -1 ){
            end++;
        }

        // 页码显示范围
        for ( let i = start; i <= end; i++ ){
            data.list.push(
                {
                    title    : i,
                    value    : i,
                    active   : i === options.pageNum
                }
            );
        }

        // 当最后页码 小于 总页数减-的时候出现断点
        if ( end < options.pages - 1 ){
            // break
            data.list.push(
                {
                    break    : true
                }
            );
            // 最后一页
            data.list.push(
                {
                    title    : options.pages,
                    value    : options.pages
                }
            );
        }

        // 下一页按钮
        data.list.push(
            {
                btn      : true,
                title    : '下一页>',
                value    : this.options.nextPage,
                disabled : !this.options.hasNextPage
            }
        );


        return Util.render( template, data );
    }
}




export default new Pagination();