/*
* @Author: comely-fox
* @Date:   2018-05-24 21:41:37
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-25 10:44:07
*/

import './index.scss';
import Util from '../blog.js';
import template from  './index.string';


class Tab {

    constructor(){

        this.id = 0;
    }

    render( $contianer ) {

        this.contianer = $contianer;

        $( this.contianer ).html( template );

        this.bindEvnet( $contianer );
    }

    bindEvnet(){

        const that = this;
        // 给选项卡添加事件委托
        $( this.contianer ).on( 'click', '.tab-tag', function(){
            // 选项卡切换
            that.toggleTab( that.getId( $( this ) ) );
        } );
        $( this.contianer ).on( 'mouseover', '.tab-tag', function( ev ){
            $( this ).find( '.close' ).stop().fadeIn( 150 );
        } );
        $( this.contianer ).on( 'mouseout', '.tab-tag', function( ev ){
            $( this ).find( '.close' ).stop().fadeOut( 150 );
        } );

        // 关闭选项卡
        $( this.contianer ).on( 'click', '.close', function( ev ){
            const id = that.getId( $( this ).parent() );

            // 关闭选项卡
            that.close( id, ( $item ) => {

                // 自动切换
                that.autoToggle( $( '.tab-tag' ), $item );
            } );

            // 重新计算选项卡宽度
            that.computeTabWidth( $( '.tab-tag' ) );

            // 阻止冒泡
            ev.stopPropagation();
        } );
    }

    /**
     * [addTab 添加选项卡]
     * @param { Object   } $obj    [ jquery元素对象]
     * @param { String   } title   [ 选项卡标题 ]
     * @param { Function } handler [ 回调函数 包含回调函数 ]
     */
    addTab( $obj, title, handler ){

        // 回调不传函数
        // 默认渲染选项卡标题
        if ( typeof handler !== 'function' ){

            handler = function( id, callback ){

                // 渲染选项卡
                callback( true );
            }
        }

        // 第一次渲染时分配id索引
        const id = this.getId( $obj ) !== '' ? this.getId( $obj ) : this.id++;

        // 添加的选项卡
        let $oTagElem = null;

        // 由处理者, 回调确定是否要渲染
        handler( id, ( success ) => {

            // 回调的渲染函数不传参数, 默认渲染
            success = success === void 0 ? true : success;

            if ( success ){

                // 选项卡没有打开的时候 建立新的选项卡
                if ( !this.isOpen(  $obj ) ){

                    // 移除所有打开的样式
                    $( '.tab-tag' ).removeClass( 'active' );

                    // 添加选项卡
                    const sTagText = `
                        <a class="tab-tag active" title="${title}" href="javascript:">
                            ${title}
                            <i class="close fa fa-close"></i>
                        </a>
                    `;

                    $oTagElem = $( sTagText );

                    // 设置多个元素为打开状态
                    this.setOn( $obj, $oTagElem );

                    // 设置多个元素的ID标识
                    this.setId( id, $obj, $oTagElem );

                    // 添加选项卡标签到页面容器
                    $( '.tab-tag-wrap' ).append( $oTagElem );

                }

                // 选项卡已经有创建过, 不再进行创建只切换
                else{

                    // 选项卡切换
                    this.toggleTab( this.getId( $obj ) );
                }

                // 计算每个选项卡的宽度
                this.computeTabWidth( $( '.tab-tag' ) );
            }

        });

        return $oTagElem;
    }

    /**
     * [ matchId 根本某一个id去匹配对应的jquery元素 ]
     * @param  { $Objects } $oElems [ 多个元素的jquery对象 ]
     * @param  { Number   } id      [ id标识 ]
     */
    matchId( $oElems, id, callback ){

        $oElems.each( function( index ){
            if ( $( this ).data( 'id' ) == id ){
                callback.call( $( this ), $( this ) , index, id );
                return false;
            }
        } );
    }

    /**
     * [toggleTab 选项卡切换]
     * @param  { Number } id [ 当前元素ID标识 ]
     */
    toggleTab( id, $objTabTag, $objTabContent ){
        const that           = this;

        $objTabTag     = $objTabTag     || $( '.tab-tag' );
        $objTabContent = $objTabContent || $( '.tab-content' );

        $objTabTag.removeClass( 'active' );
        // 置选项卡内容区全部隐藏
        $objTabContent.hide();

        this.matchId( $objTabTag, id, function(){
            $( this ).addClass( 'active' ).show();
            that.setOn( $( this ) ); // 置选项卡为打开状态
        } );
        this.matchId( $objTabContent, id, function(){
            $( this ).show( );
        } );
    }

    /**
     * [ close 关闭选项卡 ]
     * @param  { Number   } id   [ 某个id索引 ]
     * @param  { Function } id   [ 回调函数 ]
     */
    close( id, callback ){
        const that           = this;
        const $objTabTags     = $( '.tab-tag' );
        const $objTabContents = $( '.tab-content' );

        // 匹配选项卡标签元素
        this.matchId( $objTabTags, id, function(){
            $( this ).hide( ); // 隐藏选项卡

            // 有回调时执行关闭后的回调操作
            typeof callback === 'function' &&
                callback( $( this ) );
            that.setOff( $( this ) ); // 置选项卡为关闭状态
        } );

        // 匹配选项卡内容元素
        this.matchId( $objTabContents, id, function(){
            $( this ).hide( );
        } );
    }

    // 关闭一个选项卡自动判断是否打开新的选项卡
    autoToggle( $tabTagElems, $activeElem ){
        // 打开状态且是焦点的时候进行切换操作
        if ( $activeElem.hasClass( 'active' ) ){
            const length    = this.getOpenElems( $tabTagElems ).length;
            // 返回元素在打开集合中的索引
            const index     = this.getOpenElemsIndexOf( $tabTagElems, $activeElem );

            /** * next 下一步切换 * **/

            // 得到当前元素的在打开集合中中下一个元素
            const $nextElme = this.charAtOpenElems( $tabTagElems, index + 1 );

            // 得到下一个元素的id标识
            const nextId    = this.getOpenElemId( $nextElme );

            /** * 上一步切换 * **/

            // 得到当前元素的在打开集合中中上一个元素
            const $prevElme = this.charAtOpenElems( $tabTagElems, index - 1 );

            // 得到上一个元素的id标识
            const prevId    = this.getOpenElemId( $prevElme );

            const toggleId = Math.max( prevId, nextId );  // 上一步永远比下一步少, 没有下一步时,上一步比下一步大

            this.toggleTab( toggleId );
        }
    }

    /**
     * [getOpenElems 获取打开状态的元素::有data-open = true]
     * @param  { $Objects }   $objElems   [ 多个元素的jquery对象 ]
     * @param  { Function }   callback    [ 每一次循环都会调用的回调函数 [ 某一jquery对象, 当前元素索引 ] ]
     * @return { Array    }               [ 返回过滤后的数组 ]
     */
    getOpenElems( $objElems, callback ){
        const result = [];
        $objElems.each( ( index, item ) => {
            const $item = $( item );
            if ( this.isOpen( $item ) ){
                result.push( $item );
                typeof callback === 'function'
                && callback( $item, result.length - 1 );
            }
        } )
        return result;
    }

    /**
     * [getOpenElemsIndexOf 获取打开的元素集合中某一个元素的索引 ]
     * @param  { $Objects } $objElems   [ 多个元素的jquery对象 ]
     * @param  { $Object  } $objElem    [ 某一个jquery对象 ]
     * @return { Number   }             [ 返回被查找的元素索引 ]
     */
    getOpenElemsIndexOf( $objElems, $objElem ){
        const that = this;
        let index;
        this.getOpenElems( $objElems, function( obj, i ){
            if ( that.getId( obj ) === that.getId( $objElem ) ){
                index = i;
                return false;
            }
        } );
        return index;
    }

    /**
     * [charAtOpenElems 根据索引查找打开的元素集合中某jquery对象 ]
     * @param  { $Objects } $objElems   [ 多个元素的jquery对象 ]
     * @param  { Number   } index       [ 索引 ]
     * @return { $Object  }             [ 返回被查找的某一个元素的jquery对象 ]
     */
    charAtOpenElems( $objElems, index ){
        let oEle;
        this.getOpenElems( $objElems, function( obj, i ){
            if ( i === index ){
                oEle = obj;
                return false;
            }
        } );
        return oEle;
    }

    getOpenElemId( $objElems ){
        return $objElems ? this.getId( $objElems ) : -1;
    }

    /**
     * [setOff 设置某一个jquery元素为关闭状态]
     * @param { $Object } $objElem [ 某一个元素的jquery对象 ]
     */
    setOff( $objElem ){
        $objElem.data( 'open', 0 );
        $objElem.attr( 'data-open', 0 );
    }

    /**
     * [setOff 设置某一个jquery元素为打开状态]
     * @param { $Object } $objElem [ 某一个元素的jquery对象 ]
     */
    setOn( ...$objElems ){
        const values = $objElems[ Symbol.iterator ]( );
        for ( let $item of values ){
            $item.data( 'open', 1 );
            $item.attr( 'data-open', 1 );
        }
    }

    /**
     * [ getId获取jquery元素的id标识值 ]
     * @param  { $Object } $objElem [ 某一个元素的jquery对象 ]
     * @return { Number  }          [ 返回id标识值 ]
     */
    getId( $objElem ){
        return $objElem.data( 'id' );
    }

    /**
     * [setId 设置jquery元素ID标识]
     * @param { $Object } $objElem [ 某一个元素的jquery对象 ]
     * @param { Number } $objElem  [ id值 ]
     */
    setId( num, ...$objElems ){
        const values = $objElems[ Symbol.iterator ]( );
        for ( let $item of values ){
            $item.data( 'id', num );
            $item.attr( 'data-id', num );
        }
    }

    /**
     * [isOpen 检测某一个jquery元素是否是打开状态 ]
     * @param  { $Object }  $objElem   [某一个元素的jquery对象]
     * @return { Boolean }             [description]
     */
    isOpen( $objElem ){
        return !!$objElem.data( 'open' );
    }

    /**
     * [computeTabWidth 计算每个选项卡的宽度]
     * @param  { $Objects  } $elems          [ 多个元素的jquery对象 ]
     * @param  { Number    } iParentWidth    [ 父容器宽度 ]
     * @param  { Number    } defaultWidth    [ 默认每个tab标签的宽度 ]
     * @return { undefined }                 [ 无返回值 ]
     */
    computeTabWidth( $elems, iParentWidth = $( '.tab-tag-wrap' ).width(), defaultWidth = 82 ){

        // 获取打开的元素集合
        const $oTagElemsOpen = this.getOpenElems( $elems );

        //  一个打开的选项卡都没有直接返回
        if (  $oTagElemsOpen.length === 0 ) return ;

        // 取得选项卡宽度+padding+margin+border
        const iTabWidth = Math.ceil( $oTagElemsOpen[ 0 ].outerWidth( true ) );
        // 计算出margin+border+padding 多少像素
        const spacing = iTabWidth - $oTagElemsOpen[ 0 ].width() - Number.parseInt( $oTagElemsOpen[ 0 ].css( 'marginLeft' ) ) ;
        // 计算出每个元素因该占用多少宽度
        const width = Math.floor( iParentWidth / $oTagElemsOpen.length );
        // 循环设置选项卡标签
        $oTagElemsOpen.forEach( function( $item, i ){
            // 设置选项卡标签宽度 最大宽度为88
            const computeWidth = Math.min( width - spacing, defaultWidth );
            $item.css( 'width', computeWidth );
            const left =  ( computeWidth + spacing )  * i;
            $item.css( 'left', left );
        } );
    }
}

export default new Tab();