/*
* @Author: comely-fox
* @Date:   2018-05-13 14:47:51
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-28 08:35:52
*/

// 引用模板引擎
import Hogon      from 'hogan.js';

const config = {

    serverHost : ''
};
class Util {

    /**
     * [request ajax数据请求封装 ]
     * @param  { JSON   } params [ jquery.ajax参数 ]
     * @return { Object }        [ Promise 对象 ]
     */
    static request( params ){
        return new Promise( ( resolve, reject ) => {
            $.ajax( {
                url      : params.url      || '',
                type     : params.type     || 'GET',
                dataType : params.dataType || 'json',
                data     : params.data     || null,
                success  : ( res ) => {

                    switch( res.status ){

                        // 数据请求成功, 达成目的
                        case 200 :

                            resolve(  res.data );

                            break;

                        // 数据请求成功, 未达成目的
                        default :

                            reject( [ res.msg, res.status ] );

                    }
                },
                error    : ( error ) => {

                    switch( error.status ){

                        // 请求成功, 但是返回的数据不对
                        case 200 :

                            // 返回后台管理首页
                            Util.goAdmin();

                            break;

                        default :

                            reject( [ error.statusText, 0 ] );
                    }
                }

            });

        });
    }

    /**
     * [ getServerUrl 获取带主机地址的url ]
     * @param  { String } path [ 相对路径 ]
     * @return { String }      [ 返回绝对路径的URL ]
     */
    static getServerUrl( path ){

        return config.serverHost + path;
    }

    /**
     * [redirect 重定向url]
     * @param  { String } url [ url地址 ]
     */
    static redirect( url ){

        window.location.href = url;
    }

    /**
     * [goAdmin 回到admin管理页]
     */
    static goAdmin( ){

        Util.redirect( '/admin' );
    }

    /**
     * [goLogin 回到admin登录页]
     */
    static goLogin( ){

        this.redirect( '/admin/user' );
    }

    /**
     * [render 渲染html模板]
     * @param  { html } template [ 模板 ]
     * @param  { JSON } jsonData [ 数据 ]
     * @return { html }          [ 渲染完成的html ]
     */
    static render( template, jsonData ){

        const objTemplate = Hogon.compile( template );

        return objTemplate.render( jsonData );
    }

    /**
     * [turnHump 横杠转驼峰]
     * @param  {String} string [ 原始字符串 ]
     * @return {String}        [ 转换后的字符串 ]
     */
    static turnHump( string ){

        return string.replace( /-+(.)/g, ( match, group ) =>{

            return group.toUpperCase();
        });
    }

    /**
     * [turnHump 驼峰转横杠]
     * @param  {String} string [ 原始字符串 ]
     * @return {String}        [ 转换后的字符串 ]
     */
    static turnLine( string ){

        return string.replace( /([A-Z])/g, ( match, group ) =>{

            return '-' + group.toLowerCase();
        });
    }
    /**
     * [switch 轮流执行函数]
     * @param  { Function } fn1 [ 回调函数 ]
     * @param  { Function } fn2 [ 回调函数 ]
     */
    static switch( fn1, fn2 ){

        let count = 1;
        return function(){

            if ( count++ & 1 ){

                fn1();
            }

            else{

                fn2();
            }
        }
    }
    /**
     * [gradeProgress  强度进度条]
     * @param  { String } grade [ { weak, general, medium, strong } 四项强度选择 ]
     * @param  { Object } $parent [ 提示的容器  ]
     */
    static gradeProgress( grade, $parent ){

        const $progressBox = $parent.find( '.progress-box' );

        switch( grade ){

            case void 0   :
            case 'weak'   :
            case 'general':
            case 'medium' :
            case 'strong' :

            // 等级 弱
            case 'weak' :

                $progressBox.css( 'visibility', 'visible' ).find( '.progress-bar-stage-weak' ).addClass( 'active' );
                $progressBox.find( '.progress-text' ).addClass( 'weak' ).text( 'weak' );
                $progressBox.find('.progress-bar').css('transform', 'translateX(25%)');

                if ( grade === 'weak' ){

                    break;
                }

            // 等级一般
            case 'general' :

                $progressBox.css( 'visibility', 'visible' ).find( '.progress-bar-stage-general' ).addClass( 'active' );
                $progressBox.find( '.progress-text' ).addClass( 'general' ).text( 'general' );
                $progressBox.find('.progress-bar').css('transform', 'translateX(50%)');

                if ( grade === 'general' ){

                    break;
                }

            // 等级 中等
            case 'medium' :

                $progressBox.css( 'visibility', 'visible' ).find( '.progress-bar-stage-medium' ).addClass( 'active' );
                $progressBox.find( '.progress-text' ).addClass( 'medium' ).text( 'medium' );
                $progressBox.find('.progress-bar').css('transform', 'translateX(75%)');

                if ( grade === 'medium' ){

                    break;
                }

            // 等级 强
            case 'strong' :

                $progressBox.css( 'visibility', 'visible' ).find( '.progress-bar-stage-strong' ).addClass( 'active' );
                $progressBox.find( '.progress-text' ).addClass( 'strong' ).text( 'strong' );
                $progressBox.find('.progress-bar').css('transform', 'translateX(100%)');

                if ( grade === 'strong' ){

                    break;
                }

            // 没有等级强度， 不合格, 默认状态
            default :

                $progressBox.find('.progress-bar').css('transform', 'translateX(0%)');
                $progressBox.css( 'visibility', 'hidden' ).find( '.progress-text' ).attr( 'class', 'progress-text' ).text( '' );

                // 取矩阵x值
                if ( $progressBox.find('.progress-bar').css('transform').split( ', ' )[ 4 ] == 0 ){

                    $progressBox.css( 'visibility', 'hidden' )
                }

        }
    }

    /**
     * [validate 数据验证]
     * @param  { String  } value [ 需要验证的字符串 ]
     * @param  { String  } type  [ 指定的验证类型 ]
     * @return { Boolean }       [ 真, 假 ]
     */
    static validate( value, type, range = { min: 6, max: 40 } ){

        // 以字母、或者下划线开头且只能是字母数字下划线横杠组合的, 而且不能是以横杠结尾的( 4 - 16 )位用户名 比如: add-3 _Aeuw325
        const re_username = /^(?:[a-zA-Z_])(?=.{3,15})(?:[a-zA-Z0-9_-]*)(?:[a-zA-Z0-9_])$/;

        // 6位以上的密码, 必需包含大小写字母、数字、特殊字符
        // 一级密码强度
        // 有大写或者小写字母 + 数字的组合
        const re_password_weak_one    = /^(?:(?=.*[A-Z])|(?=.*[a-z]))(?=.*\d)(?:[a-z0-9]|[A-Z0-9]){6,40}$/;

        // 有大写或者小写字母 + 特殊字符的组合
        const re_password_weak_two    = /^(?:(?=.*[A-Z])|(?=.*[a-z]))(?=.*[^0-9a-zA-z])(?:[^0-9]){6,40}$/;

        // 有数字 + 特殊字符的组合
        const re_password_weak_three  = /^(?=.*\d)(?=.*[^0-9a-zA-z])(?:[^a-zA-z]){6,40}$/;

        // 二级密码强度, 有大小写字母+数字的组合
        const re_password_general_one = /^(?=.*[A-Z])(?=.*\d)(?=.*[a-z])(?:[a-zA-Z0-9]){6,40}$/;

        // 有大小写字母+特殊字符的组合
        const re_password_general_two = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[^a-zA-Z0-9])(?:[^0-9]){6,40}$/;


        // 三级密码强度, 有大写或者小写字母 + 数字的组合 + 特殊字符的组合
        const re_password_medium = /^(?=.*\d)(?=.*[a-z]?)(?=.*[A-Z]?)(?=.*[`~!@#$%^&*()\-_=+[\]{;:'"}|\\<>,./?])(?:[^a-z]{6,40})?(?:[^A-Z]{6,40})?$/;

        // 四级密码强度, 有大小写字母+数字+特殊字符的组合
        const re_password_strong  = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[`~!@#$%^&*()\-_=+[\]{;:'"}|\\<>,./?]).{6,40}$/;

        // 验证是否为空
        if ( type === 'empty' ){
            return !!value;
        }

        // 验证用户名
        if ( type === 'username' ){

            return re_username.test( value );
        }

        // 验证用户名密码
        if ( type === 'password' ){

            // 密码强度弱
            if ( re_password_weak_one.test( value ) || re_password_weak_two.test( value ) || re_password_weak_three.test( value )){

                if ( value.length > 11 ){

                    return 'general';
                }

                else{

                    return 'weak';
                }

            }

            // 密码强度一般
            else if( re_password_general_one.test( value ) || re_password_general_two.test( value ) ){

                if ( value.length > 11 ){

                    return 'medium';
                }

                else if( value.length < 8 ){

                    return 'weak';
                }

                else{

                    return 'general';
                }

            }

            // 密码强度中等
            else if( re_password_medium.test( value ) ){


                if ( value.length > 11 ){

                    return 'strong';
                }

                else if( value.length < 8 ){

                    return 'general';
                }

                else{
                    return 'medium';
                }
            }

            // 密码强度很强
            else if( re_password_strong.test( value ) ){

                if( value.length < 11 ){

                    return 'medium';
                }

                else if( value.length < 8 ){

                    return 'general';
                }

                else{

                    return 'strong';
                }

            }

            // 密码不格式不正确 error
            else {

                return false;
            }

        }
    }

    /**
     * [formValidate 储存验证表单控件数据的所有类型, 并返回验证后的结果]
     * @param  { String | JSON  } data     [ string某一个控件的值, json时要验证的控件值集体]
     * @param  { String | Array } type     [ 指定验证类型 ]
     * @param  { Boolean        } full     [ 指定是使用简单验证，还是复杂验证 ]
     * @return { Object         }          [ 验证后的结果，包含提示信息、状态、控件名,验证类型.验证通过的控件数量、 验证的类型 ]
     */
    static formDataValidate( data, type, full ){

        const result = {
            msg    : new Map(),  // 储存所有控件的提示信息与状态
            type   : [ ],        // 储存用到的验证类型
            status : false,      // 所有控件验证通过
            count  : 0           // 验证通过的控件数量
        };

        const checkFrom = new Map();

        // 验证类型为字符串时, 转为数组
        if ( typeof type === 'string' ){

            type = [ type ];
        }

        // 不是数组的其它情况,验证类型就是data对象的key集
        if( !Array.isArray( type ) ){

            type = Object.keys( data );
        }

        // data数据为字符串时,必须指定 type 验证类型, 且把data 转为json
        if ( typeof data === 'string' ){

            data = { [ type ]: data };
        }


        // 存储用户名的验证方法
        checkFrom.set( 'username', function( data ){

            // 要验证的数据
            const username = data.value;

            // result.msg Map对象的元素的key值
            const mapKey   = data.name,

                  // result.msg Map对象的元素的value值
                  mapValue = { checkType: 'username', name : mapKey, status: false, text : '' };

            // 是否验证通过
            let pass = false;

            // 验证用户名是否为空
            if ( !Util.validate( username, 'empty' ) ){

                // 提示文本信息
                mapValue.text = '用户名不能为空';

                // 添加result.msg Map对象的元素
                result.msg.set( mapKey, mapValue );

                return;
            }

            else{

                pass = true;
            }

            // 复杂验证 多验证
            if ( pass && full ){

                if( !Util.validate( username, 'username' ) ){

                    pass = false;

                    // 提示文本信息
                    mapValue.text = '用户名必须以下划线开头或者字母开头, 且不能是中间线结尾的长度为4-16位的账号';

                    // 添加result.msg Map对象的元素
                    result.msg.set( mapKey, mapValue );

                    return;
                }

                else{

                    pass = true;
                }
            }

            // 验证通过
            if ( pass ){

                // 当前验证数码状态设置通过
                mapValue.status = true;

                // 提示文本信息
                mapValue.text = '用户名验证通过';

                // 添加result.msg Map对象的元素
                result.msg.set( mapKey, mapValue );

                // 验证通过的控件数量+1
                result.count += 1;
            }

        });

        // 存储密码的验证方法
        checkFrom.set( 'password', function( data ){

            // 要验证的数据
            const password = data.value;

            // result.msg Map对象的元素的key值
            const mapKey   = data.name,

                  // result.msg Map对象的元素的value值 = 验证对象信息
                  mapValue = { checkType: 'password', name : mapKey, status: false, text : '' };

            // 提示文本信息
            switch( mapKey ){

                case 'oldPassword' :

                    mapValue.text = "原始密码";

                    break;

                case 'newPassword' :

                    mapValue.text = "新密码";

                    break;

                default :

                    // 提示文本信息
                    mapValue.text = '密码';
            }

            // 是否验证通过
            let pass = false;

            // 验证密码是否为空
            if ( !Util.validate( password, 'empty' ) ){

                // 提示文本信息
                mapValue.text += '不能为空';

                // 添加result.msg Map对象的元素
                result.msg.set( mapKey, mapValue );

                return;
            }

            else{

                pass = true;
            }

            // 复杂验证 多验证
            if ( pass && full ){

                if( !Util.validate( password, 'password' ) ){

                    pass = false;

                    // 提示文本信息
                    mapValue.text += '范围必需是6-40位, 且至少有大写或者小写字母 + 数字的组合';

                    // 添加result.msg Map对象的元素
                    result.msg.set( mapKey, mapValue );

                    return;
                }

                else{

                    pass = true;
                }
            }

            // 验证通过
            if ( pass ){

                // 当前验证数码状态设置通过
                mapValue.status = true;

                // 提示文本信息
                mapValue.text += '验证通过';

                // 设置密码强度等级
                mapValue.grade =  Util.validate( password, 'password' );

                // 添加result.msg Map对象的元素
                result.msg.set( mapKey, mapValue );

                // 验证通过的控件数量+1
                result.count += 1;
            }

        });

        // 存储确认密码的验证方法
        checkFrom.set( 'password2', function( data ){

            // 输入的密码
            const password  = data.password,

                  // 重复输入的密码
                  password2 = data.password2;


            // result.msg Map对象的元素的key值
            const mapKey   = data.name,

                // result.msg Map对象的元素的value值 = 验证对象信息
                mapValue = { checkType: 'password2', name : mapKey, status: false, text : '' };


            // 验证确认密码是否为空
            if ( !Util.validate( password2, 'empty' ) ){

                // 提示文本信息
                mapValue.text = '确认密码不能为空';

                // 添加result.msg Map对象的元素
                result.msg.set( mapKey, mapValue );
            }

            // 验证确认密码是否一致
            else if( password !== password2 ){

                // 提示文本信息
                mapValue.text = '两次输入的密码不一致';

                // 添加result.msg Map对象的元素
                result.msg.set( mapKey, mapValue );
            }

            // 验证通过
            else {

                 // 当前验证数码状态设置通过
                mapValue.status = true;

                // 提示文本信息
                mapValue.text = '确认密码验证通过';

                // 添加result.msg Map对象的元素
                result.msg.set( mapKey, mapValue );

                // 验证通过的控件数量+0
                result.count += 1;
            }

        });


        // 储存运行验证方法
        checkFrom.set( 'run', function(){

            // 修改type数组长度
            type.length = Object.keys( data ).length;

            // 遍历检测类型 type 数组
            type.forEach( ( key, index ) => {

                // 设置检测方法的参数 { name : data对象数据属性名, value : 对象属性值 }
                // 1.如果 data对象数据有type检测类型的某一个值, 则直接查找对应的data对象数据的属性值
                // 2.否则 以type检测类型的索引顺序来取data对象数据的属性名与属性值
                let oData = data[ key ] ? {

                                            name: key,
                                            value : data[ key ]
                                          }
                                        : {

                                            name : Object.keys( data )[ index ],
                                            value : Object.values( data )[ index ]
                                          };
                // 检测类型 type数组里 有password2检测类型
                if ( key === 'password2' ){

                    // 确认密码, 重复密码
                    const password2 = oData.value;

                    // 设置的密码
                    // 1.截取确认密码, 重复密码的key
                    const key = oData.name.substring( 0, oData.name.length - 1 );

                    // 2.取出data对象数码里截取的key对应的属性值
                    // 3.否则取出与type检测类型数组对应的data对象数据的上一条属性值
                    const password = data[ key ] || Object.values( data )[ index -1 ];

                    // 重新设置oData对象
                    oData = { name : oData.name, password, password2 };

                }

                // 检测checkFrom Map对象是否有这个成员
                if ( checkFrom.has( key ) ){

                    checkFrom.get( key )( oData );
                }

            });

            // 修改验证类型
            result.type = type;

        });


        // 运行验证
        checkFrom.get( 'run' )();


        // 检测所有验证是否都通过
        if ( result.count ===  type.length ){

            // 所有状态为验证成功
            result.status = true;
        }

        return result;
    }

    /**
     * [formValidateResult 对formDataValidate方法返回的结果信息map进行循环处理 ]
     * @param  { Object } $parent [ 提示的容器  ]
     * @param  { Object } result  [ formValidate方法验证后的Map对象结果 ]
     * @param  { Object } exec    [ 回调对象，包含{ success, error, process } 方法 ]
     */
    static validateResult( result, exec ){

        // 遍历result.msg Map对象, key 就是验证类型, value 验证返回的结果
        for( let [ key, value ] of result.entries() ){

            // 控件验证成功
            if ( value.status ){

                // 验证成功执行的回调
                typeof exec.success === 'function' && exec.success.call( result, value.name, value, value.checkType );
            }

            // 控件验证失败
            else{

                // 验证失败执行的回调
                typeof exec.error === 'function' && exec.error.call( result, value.name, value, value.checkType );

            }

            // 验证过程中执行的回调
            typeof exec.process === 'function' && exec.process.call( result, value.name, value, value.checkType );

        }
    }

    /**
     * [dataValidateAndTip 验证表单控件数据]
     * @param  { JSON } json [ { data, type, full, onError, onSuccess, onProcess, allSuccess, allError } ]
     */
    static formValidate( json ){

        const validateResult = Util.formDataValidate( json.data, json.type, json.full );

        Util.validateResult( validateResult.msg, { success: json.onSuccess, error: json.onError, process : json.onProcess } );

        // 表单验证成功
        if ( validateResult.status ){

            typeof json.allSuccess === 'function' && json.allSuccess( json.data );
        }

        // 表单验证失败
        else {

           typeof json.allError === 'function' && json.allError();
        }
    }

    /**
     * [setData 设置元素data]
     * @param { Ojbect } $elem [ jquery对象 ]
     * @param { String } key   [ 键名 ]
     * @param { String } value [ 键值 ]
     */
    static setData( $elem, key, value ){

        $elem.data( key, value );
        $elem.attr( 'data-' + key, value );

        return $elem;
    }

    /**
     * [getData 获取元素data值]
     * @param  { Ojbect } $elem [ jquery对象 ]
     * @param  { String } key   [ 键名 ]
     * @return { String } value [ 键值 ]
     */
    static getData( $elem, key ){

        return $elem.data( key );
    }

    /**
     * [ cookies 设置cookie 或者获取cookie值 ]
     * @param  { String } key   [ 键名 ]
     * @param  { String } value [ 键值 ]
     * @return {[type]}       [description]
     */
    static cookies( key, value ){
        let k, v, result, re_pattern, expires;

        // 多组cookie键对递归调用
        if ( typeof key === 'object' ){

            for ( k in key ){

                v = key[ k ];
                Util.cookies( k, v );
            }
        }

        // 设置cookie
        else if( value != void 0 ){

            document.cookie = `${key}=${value};expires=expires`;
        }

        // 获取cookie
        else {

            re_pattern = /(\w+)=([^;\s]+)/g;

            while ( result = re_pattern.exec( document.cookie ) ){

                if ( result[ 1 ] === key ){

                    value = result[ 2 ]
                    break;
                }
            }

            return value;
        }
    }

    /**
     * [licence 权限检测]
     * @param  { String  } identify [ query, add, edit, delete ]
     * @return { Boolean }          [ true, false ]
     */
    static licence( identify ){

        switch( identify ){

            case 'query' :

                return !!+Util.cookies( 'queryable' );

                break;

            case 'add' :

                return !!+Util.cookies( 'addable' );

                break;

            case 'edit' :

                return !!+Util.cookies( 'editable' );

                break;

            case 'delete' :

                return !!+Util.cookies( 'deletable' );

                break;

            case 'root' :

                return !!+Util.cookies( 'isRoot' );

        }
    }

    /**
     * [isLoginUser 检测当前账号是否是登录帐号]
     * @param  { String  }  username [ 要被检测的用户名 ]
     * @return { Boolean }           [ 真, 假 ]
     */
    static isLoginUser( username ){

        return Util.cookies( 'username') === username;
    }

    /**
     * [ getCheckedValue 获取checkbox 控件是否是选中状态 ]
     * @param  { Array } items [ 类数组, checkbox控件元素集合 ]
     * @return { Array }       [ 所有被检测的checkbox控件状态集合 ]
     */
    static getCheckedValue( items ){

        const aResult = [ ];
        Array.from( items ).forEach( ( item, index ) => {

            let val = 0;

            if ( item.checked ){

                val = 1;
            }

            aResult.push( val );

        } );

        return aResult;
    }

    /**
     * [inputEvent  绑定输入框的默认事件 ]
     * @param  { Object } $elems   [ jquery元素对象 ]
     * @param  { String } selector [ 选择器, 可包含需要验证的控件类型, 需要同步一样的名称 ]
     * @param  { String } type     [ 检测类型 ]
     * @param  { Object } cb       [ 事件验证的回调 ]
     */
    static bindDefaultEvent( $elems, selector, type, eventCb ){

        const that = this;

        // 没有传入$elems的时候
        if ( typeof $elems === 'string' ){

            [ $elems, selector, type, eventCb ] = [ $( 'body' ), $elems, selector, type ];
        }

        // 提取出父级选择器
        // 1. 例 .root.for1 .item .password => root.for1 .item
        let parentselector = selector.substring( 0, selector.lastIndexOf( ' ' ) );

        // 父级选择器没有带子选择器, type 检测类型必须传入
        if ( parentselector === '' ){

            parentselector = selector;

            selector = parentselector + ' .' + type;

        }

        // 检测类型为字符串时
        // type selector 转为数组
        if ( typeof type === 'string' ) {

            type = [ type ];

            selector = [ selector ];
        }

        // 检测类型为数组时
        // 例 ( parentselector, [ type1, type2, ... ] );
        else if ( Array.isArray( type ) ){

            selector = type.reduce( ( accumulator, name ) => {

                accumulator.push( parentselector + ' .' + name );

                return accumulator;
            }, [ ] );
        }

        // type 检测类型为其它情况时,[ undefined ... ], selector 选择器必须有带检测类型的子选择器
        // 例 ( parentselector + childSelector );
        else {

            // 提取出要验证的控件的类型
            type = [ selector.substring( selector.lastIndexOf( '.' ) + 1 ) ];

            selector = [ selector ];
        }

        if ( !type ){

            // throw new Error( '必须传入检测类型' );
        }

        // type, selector 最终结果都要转为数组形式


        selector.forEach( ( childSelector, index ) => {


            // 检测类型
            const type2 = type[ index ];

            // 提取控件名
            // 1.原始控件名 例: old-password
            const originName = childSelector.substring( childSelector.lastIndexOf( '.' ) + 1 );

            // 2.转成驼峰形式
            const name = that.turnHump( originName );

            // 绑定输入框字符被改变时事件
            $elems.on( 'input', childSelector, { callback: eventCb.onInput }, function( ev, cb ){

                const $this   = $( this );

                // 获取触发监听器的元素的唯一父级
                const $parent = $this.parents( parentselector );

                let data = { [ name ]: $this.val().trim() };

                if ( type2 === 'password2' ){

                    // 提取确认密码控件名的第一次输入的密码控件名
                    // 1.第一次输入的密码控件名源名
                    const originPasswordName = originName.substring( 0, originName.length - 1 );

                    // 2.转成驼峰形式
                    const passwordName = that.turnHump( originPasswordName );

                    // 增加第一次输入的密码
                    // 1.查找同级下是否有此元素
                    // 2.否则在上一层父级查找
                    data[ passwordName ] = $parent.find( ':input.' + originPasswordName ).val() || $parent.parent().find( ':input.' + originPasswordName ).val();

                }

                // 调用处理事件的回调方法
                // callback( $parent, data, type2, name )
                // $parent = jquery元素对象类型
                // data    = json对象类型
                // type2   = string类型
                // name    = string类型
                // this    = 触发事件监听器的dom元素

                let callback = cb || ev.data.callback;

                const returnObj = typeof callback === 'function' && callback.call( this, $parent, data, type2, name );

                // 检测回调函数的返回值是否是Promise对象
                typeof returnObj === 'object' && returnObj.then && returnObj.then( eventCb.resolve, eventCb.reject );

            });

            const e = jQuery.Event('input');

            // 绑定控件获取焦点时的事件
            $elems.on( 'focus', childSelector, { callback: eventCb.onFocus }, function( ev ) {

                // 防止没有回调的时候, 调用onInput处理事件的回调方法
                const callback = ev.data.callback || 1;

                // 手动触发input事件
                $( this ).trigger( e, callback );
            });

            // 绑定控件失去焦点时的事件
            $elems.on( 'blur', childSelector, { callback: eventCb.onBlur }, function( ev ) {

                // 防止没有回调的时候, 调用onInput处理事件的回调方法
                const callback = ev.data.callback || 1;

                // 手动触发input事件
                $( this ).trigger( e, callback );
            });

        });



        // function handler( ev, type2 )
    }

    /**
     * [ resetInput 重置输入控件为初始状态 ]
     * @param  { Ojbect   } $elems        [ 所有input控件的共同父级元素容器 ]
     * @param  { String   } childSelector [ 重置按钮 ]
     * @param  { Array    } defaultStr    [ 默认的input控件字符 ]
     * @param  { Function } callback      [ 重置完成后的回调函数 ]
     */
    static resetInput( $elems, childSelector, defaultStr, callback ){

        if ( typeof $elems === 'string' ){

            [ $elems, childSelector, defaultStr, callback ] = [ $( 'body' ), $elems, childSelector, defaultStr ];
        }

        // 提取出父级容器
        let parentselector = childSelector.substring( 0, childSelector.indexOf( ' ' ) );

        defaultStr = defaultStr === void 0 ? '' : defaultStr;

        if ( typeof defaultStr === 'function' ){

            [ callback, defaultStr ] = [ defaultStr, '' ];
        }

        // 重置按钮被单击时执行事件
        $elems.on( 'click', childSelector, function(){

            const $this   = $( this );

            let   $parent = $this.parent( );

            const $inputs = (function walk( $p, inputs ) {

                inputs =  $p.find( ':text,:password' );

                $parent = $p;

                if ( inputs.length === 0 ){

                   inputs =  walk( $p.parent() );
                }

                return inputs;
            })( $parent );

            // 重置完后返回的所有的默认文本
            const datas = [ ];

            $inputs.each( function( index, item ){

                const $this = $( this );

                const string = defaultStr[ index ] || defaultStr;

                // 给input填充默认数据
                $this.val( defaultStr[ index ] || defaultStr );

                datas.push( string );
            });

            // 重置完成后的回调函数
            // callback( $parent, data, type2, name )
            // $parent = jquery元素对象类型
            // $inputs = jQuery控件集合
            // datas   = Array
            // this    = $parent
            typeof callback === 'function' && callback.call( $parent, $parent, $inputs, datas );
        });
    }

    /**
     * [passView 密码可视切换]
     * @param  { Ojbect }               [ JQuery 元素对象 ]
     * @param  { String } childSelector [ 子选项选择器 ]
     */
    static passView( $elem, childSelector ){

        if ( typeof $elem === 'string' ){

            [ $elem, childSelector ] = [ $( 'body' ), $elem ];
        }

        $elem.on( 'click', childSelector, function(){

            const $this = $( this );

            if ( !$this.data( 'switch' ) ){

                $this.siblings( 'input' ).attr( 'type', 'text' );
                $this.removeClass( 'fa-eye-slash' ).addClass( 'fa-eye' );

                $this.data( 'switch', 1 );
            }

            else{

                $this.siblings( 'input' ).attr( 'type', 'password' );
                $this.removeClass( 'fa-eye' ).addClass( 'fa-eye-slash' );

                $this.data( 'switch', 0 );
            }

            return false;
        } );
    }
}

export default Util;