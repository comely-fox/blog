/*
* @Author: comely-fox
* @Date:   2018-05-12 16:16:51
* @Last Modified by:   comely-fox
* @Last Modified time: 2018-06-28 09:01:27
*/

const webpack           = require( 'webpack' );
const path              = require( 'path' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const ExtractTextPlugin = require('extract-text-webpack-plugin');

// 环境变量配置, dev / online
var WEBPACK_ENV = process.env.WEBPACK_ENV || 'dev';

// 获取html-webpack-plugin 参数的方法
const getHtmlConfig = function( name, title ){
    return {
        template : './src/admin/views/' + name + '.html',
        filename : path.resolve( __dirname, 'server/views/admin/' ) + '/' + name + '.art',
 /*       favicon  : './favicon.ico',*/
        title    : title,
        inject   : true,
        hash     : true,
        chunks   : [ 'common', name ],
       /* minify   : {
            collapseWhitespace: true
        }*/
    }
}

const config = {
    entry : {
        'jQuery' : [ './src/util/jquery.js' ],
        'common' : [ './src/admin/page/common/index.js' ],
        'index'  : [ './src/admin/page/index/index.js' ],
        'login'  : [ './src/admin/page/login/index.js' ]
    },
    output: {
        path: path.resolve( __dirname, 'server/statics/admin' ),
        publicPath : '/admin/',
        filename: 'js/[name].js'
    },
    externals: {

        // html 引入
        // 'jquery': 'window.jQuery'
    },
    resolve : {
        alias : {
            node_modules : path.resolve( __dirname, 'node_modules' ),
            util         : path.resolve( __dirname, 'src/util' ),
            page         : path.resolve( __dirname, 'src/admin/page' ),
            interface    : path.resolve( __dirname, 'src/admin/interface' )/*,
            image        : path.resolve( __dirname, 'src/image' )*/
        }
    },
    module :{
        rules : [
            {

                // jquery打包以后
                // 设置jquery为全局变量
                test : require.resolve( './src/util/jquery.js' ),
                use: [
                    {
                        loader: 'expose-loader',
                        options: 'jQuery'
                    },
                    {
                        loader: 'expose-loader',
                        options: '$'
                    }
                ]
            },
            {
                test : /\.scss$/,
                use : ExtractTextPlugin.extract( {
                    use : [ 'css-loader', 'sass-loader' ]
                } )
            },
            {
                test: /\.(gif|png|jpg|woff|svg|eot|ttf)\??.*$/,
                use: 'url-loader?limit=100&name=resource/[name].[ext]'
            },
            {
                test: /\.string*$/,
                use: 'html-loader'
            }
        ]
    },
    plugins : [
        // 提出公共部分
        new webpack.optimize.CommonsChunkPlugin({
            name        : ['common', 'jQuery'],
            filename    : 'js/[name].js',
            minChunks   : 2
        }),
         // 把css 单独打包到文件
        new ExtractTextPlugin( 'css/[name].css' ),

        new HtmlWebpackPlugin( getHtmlConfig( 'login', '管理员登录' ) ),
        new HtmlWebpackPlugin( getHtmlConfig( 'index', '后台管理中心' ) )
    ],
    devServer : {
        proxy: {
            '/' : {
                target : 'http://localhost:3000/'
            }
        }
    }
};




/*if ( WEBPACK_ENV === 'dev' ){
    config.entry.common.push( 'webpack-dev-server/client?http://localhost:8080/' );
}*/

module.exports = config;